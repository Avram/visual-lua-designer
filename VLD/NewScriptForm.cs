﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace VLD
{
    public partial class NewScriptForm : Form
    {

        private String tplpath;


        public NewScriptForm()
        {
            InitializeComponent();

            this.tplpath = Program.exedir + @"\data\templates";

        }


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
                return;

            String descline;
            String fname = listBox1.SelectedItem.ToString()+@".xml";

            using (StreamReader reader = new StreamReader(this.tplpath+@"\"+fname))
            {
                descline = reader.ReadLine();
            }

            textBox1.Text = descline.Replace("<!--", "").Replace("-->", "").Trim();
        }

        private void NewScriptForm_Load(object sender, EventArgs e)
        {
            DirectoryInfo dinfo = new DirectoryInfo(this.tplpath);
            
            //read template files only if template directory exists
            if (dinfo.Exists)
            {
                FileInfo[] Files = dinfo.GetFiles("*.xml");
                foreach (FileInfo file in Files)
                {
                    listBox1.Items.Add(file.Name.Replace(".xml", ""));
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex < 0)
            {
                MessageBox.Show("No template selected!");
                return;
            }

            String fname = listBox1.SelectedItem.ToString()+@".xml";
            String xml = "";
            String[] lines = File.ReadAllLines(this.tplpath + @"\" + fname);
            for (int i = 1; i < lines.Length; i++)
                xml += lines[i];

            Object[] args = new Object[1];
            args[0] = xml;
            MainForm.getInstance().getBrowser().Document.InvokeScript("clearXML");
            MainForm.getInstance().getBrowser().Document.InvokeScript("loadXML", args);

            MainForm.getInstance().scriptName = "script";
            MainForm.getInstance().fileName = "";
            MainForm.getInstance().updateTitle();

            // webBrowser1.Document.InvokeScript("clearXML");

            Close();
        }


    }
}
