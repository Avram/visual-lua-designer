﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace VLD
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public partial class MainForm : Form
    {
        public string scriptName = "";
        public string fileName = "";
        Timer timer;
       // string initXML = "<xml xmlns=\"http://www.w3.org/1999/xhtml\"><block type=\"procedures_defnoreturn\" y=\"-7\" x=\"-8\"><mutation><arg name=\"e\" /></mutation><title name=\"NAME\">script_init</title></block><block type=\"procedures_defnoreturn\" y=\"75\" x=\"-7\"><mutation><arg name=\"e\" /></mutation><title name=\"NAME\">script_main</title><statement name=\"STACK\"><block inline=\"false\" type=\"controls_if\"><value name=\"IF0\"><block inline=\"true\" type=\"logic_compare\"><title name=\"OP\">LTE</title><value name=\"A\"><block inline=\"true\" type=\"fpsc_playerdistance\"><value name=\"ENTITY\"><block type=\"fpsc_entity\" /></value></block></value><value name=\"B\"><block type=\"math_number\"><title name=\"NUM\">200</title></block></value></block></value><statement name=\"DO0\"><block inline=\"true\" type=\"fpsc_prompt\"><value name=\"TEXT\"><block type=\"text\"><title name=\"TEXT\">Hello there!</title></block></value></block></statement></block></statement></block></xml>";
        Config config = null;
        //int IEver = 0x02710;
        String mainHtmlFile = @"\data\apps\code\index.html";

        private static MainForm instance = null;

        public static MainForm getInstance()
        {
            if (MainForm.instance == null)
                MainForm.instance = new MainForm();

            return MainForm.instance;
        }


        private MainForm()
        {
            InitializeComponent();

            if (File.Exists(Program.exedir + @"\config.ini"))
            {   
                //Comment this line because its easier to load from the HTML File, directly after Blockly Load.  Check data\apps\code\js\frame_scripts.js
               /*
                try
                {  
                    this.config = Config.getInstance("config.ini");
                    this.IEver = Int32.Parse(this.config.get("IE")) * 1000;
                    string init_empty = this.config.get("init_empty");
                    if (init_empty.Equals("0"))
                    {
                        if (File.Exists(Program.exedir + @"\init.xml"))
                            this.initXML = File.ReadAllText(Program.exedir + @"\init.xml");
                    }
                    else 
                        this.initXML = "<xml xmlns=\"http://www.w3.org/1999/xhtml\"><block type=\"procedures_defnoreturn\" y=\"-7\" x=\"-8\"><mutation><arg name=\"e\" /></mutation><title name=\"NAME\">script_init</title></block><block type=\"procedures_defnoreturn\" y=\"75\" x=\"-7\"><mutation><arg name=\"e\" /></mutation><title name=\"NAME\">script_main</title></block></xml>";
               
            }
            catch (Exception e) { }*/


            }

            string executablePath = Environment.GetCommandLineArgs()[0];
            string executableName = System.IO.Path.GetFileName(executablePath);
            

           // RegistryKey registrybrowser = Registry.CurrentUser.OpenSubKey
           //    (@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", true);

           // if (registrybrowser == null)
           // {
           //     RegistryKey registryFolder = Registry.CurrentUser.OpenSubKey
           ///         (@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl", true);
           //     registrybrowser = registryFolder.CreateSubKey("FEATURE_BROWSER_EMULATION");
          //  }
            
           // registrybrowser.SetValue(executableName, this.IEver, RegistryValueKind.DWord);
            //registrybrowser.SetValue(executableName, 0x02710, RegistryValueKind.DWord);
            //registrybrowser.SetValue(executableName, 0x2328, RegistryValueKind.DWord);
            //registrybrowser.Close();

            //find fpscr
            RegistryKey unin = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\The Game Creators\FPSCRELOADED", false);

            if (unin == null)
                unin = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\The Game Creators\FPSCRELOADED", false);

            if (unin != null)
            {
                Program.fpscdir = ((String)unin.GetValue("INSTALL-PATH")).Trim();
                unin.Close();
            }
            else
            {
                Program.fpscdir = Environment.GetEnvironmentVariable("ProgramFiles") + "\\The Game Creators\\FPS Creator Reloaded";
            }

            if (!Directory.Exists(Program.fpscdir))
                Program.fpscdir = "";

        }

        private void MainForm_Show(object sender, EventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();

            if (args.Length == 2)
            {
                string fname = args[1];
                if (File.Exists(fname))
                {
                    this.scriptName = Path.GetFileNameWithoutExtension(fname);
                    this.fileName = fname;
                }
             
            }

            String vldpath = Program.exedir + mainHtmlFile; //@"\data\vld.html";

            if (!File.Exists(vldpath))
                vldpath = "d:\\Dropbox\\fpsc visual lua creator desktop\\client\\vld.html";

            //webBrowser1.Navigate("http://" + host);
            webBrowser1.ObjectForScripting = this;
            webBrowser1.Navigate("file:///"+vldpath+"?rand="+new Random().Next(1,99999));

            this.updateTitle();
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            //webBrowser1.Document.Body.MouseDown += Body_MouseDown;
            webBrowser1.ObjectForScripting = this;

            this.timer = new Timer();
            this.timer.Interval = 500;
            this.timer.Tick += timer_Tick;
            this.timer.Start();


            //HtmlElement el = webBrowser1.Document.GetElementById("simple-menu");
            //el.OuterHtml="test";

            
        }

        public WebBrowser getBrowser()
        {
            return this.webBrowser1;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!this.fileName.Equals(""))
            {
                this.loadFile(this.fileName);
            }
            else
            {
                Object[] args = new Object[1];
                //args[0] = this.initXML;
              //  webBrowser1.Document.InvokeScript("clearXML");
               // webBrowser1.Document.InvokeScript("loadXML", args);
            }

            this.timer.Stop();
        }


        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.fileName.Equals(""))
            {
                this.saveAsToolStripMenuItem_Click(sender, e);
            }
            else
            {
                this.saveFile(this.fileName.ToLower());
            }

            this.updateTitle();
        }

        private void saveFile(String filename)
        {
            //MessageBox.Show(filename);
            Object[] args = new Object[1];
            args[0] = String.Format(filename.ToLower());
            webBrowser1.Document.InvokeScript("gimmeXML", args);
        }

        private bool loadFile(String filename)
        {
            String[] lines = File.ReadAllLines(filename);
            String pattern = @"--\s?VLD:\s?(.*)$";
            String b64 = null;

            foreach (String line in lines)
            {
               if (Regex.IsMatch(line, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace))
               {
                   Match mc = Regex.Match(line, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                   b64 = mc.Groups[1].Value;
                   break;
               }
            }

            if (b64 == null)
            {
                MessageBox.Show("File "+Path.GetFileName(filename)+" does not contain Visual LUA Designer information and thus can not be loaded!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            Object[] args = new Object[1];
            args[0] = Encoding.UTF8.GetString(Convert.FromBase64String(b64));
            webBrowser1.Document.InvokeScript("clearXML");
            webBrowser1.Document.InvokeScript("loadXML", args);
            return true;
        }

        public void hereItIs(String xml, String lua, String filename)
        {

            //if scriptname is not set, get it from function name (script_main(e))
            if (this.scriptName.Equals(""))
            {
                string pattern = @"function\s+([a-zA-Z0-9_]+)_main\(e\)";
                if (Regex.IsMatch(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace)) {
                    this.scriptName = Regex.Match(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace).Groups[1].Value.ToLower();
                } else {
                    this.scriptName = "script";
                }
            }//*/

            //file name without extension
            string fname = Path.GetFileNameWithoutExtension(filename).ToLower();

            //if filename is different than function name, rename functions
            if (!fname.Equals(this.scriptName))
            {
                lua = Regex.Replace(lua, this.scriptName + "_main", fname + "_main", RegexOptions.IgnoreCase);
                lua = Regex.Replace(lua, this.scriptName + "_init", fname + "_init", RegexOptions.IgnoreCase);
                lua = Regex.Replace(lua, this.scriptName + "_exit", fname + "_exit", RegexOptions.IgnoreCase);
                xml = Regex.Replace(xml, this.scriptName + "_main", fname + "_main", RegexOptions.IgnoreCase);
                xml = Regex.Replace(xml, this.scriptName + "_init", fname + "_init", RegexOptions.IgnoreCase);
                xml = Regex.Replace(xml, this.scriptName + "_exit", fname + "_exit", RegexOptions.IgnoreCase);
            }
            //force lowercase in function names
            else
            {
                string pattern = @"(function\s+([a-zA-Z0-9_]+_main))\(e\)";
                if (Regex.IsMatch(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace))
                {
                    string luaname = Regex.Match(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace).Groups[1].Value.ToLower();
                    string xmlname = Regex.Match(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace).Groups[2].Value.ToLower();

                    lua = Regex.Replace(lua, luaname, luaname.ToLower(), RegexOptions.IgnoreCase);
                    xml = Regex.Replace(xml, xmlname, xmlname.ToLower(), RegexOptions.IgnoreCase);
                }

                pattern = @"(function\s+([a-zA-Z0-9_]+_init))\(e\)";
                if (Regex.IsMatch(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace))
                {
                    string luaname = Regex.Match(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace).Groups[1].Value.ToLower();
                    string xmlname = Regex.Match(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace).Groups[2].Value.ToLower();

                    lua = Regex.Replace(lua, luaname, luaname.ToLower(), RegexOptions.IgnoreCase);
                    xml = Regex.Replace(xml, xmlname, xmlname.ToLower(), RegexOptions.IgnoreCase);
                }

                pattern = @"(function\s+([a-zA-Z0-9_]+_exit))\(e\)";
                if (Regex.IsMatch(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace))
                {
                    string luaname = Regex.Match(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace).Groups[1].Value.ToLower();
                    string xmlname = Regex.Match(lua, pattern, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.IgnorePatternWhitespace).Groups[2].Value.ToLower();

                    lua = Regex.Replace(lua, luaname, luaname.ToLower(), RegexOptions.IgnoreCase);
                    xml = Regex.Replace(xml, xmlname, xmlname.ToLower(), RegexOptions.IgnoreCase);
                }

            }

            //update XML in editor
            webBrowser1.Document.InvokeScript("clearXML");
            Object[] args = new Object[1];
            args[0] = xml;
            webBrowser1.Document.InvokeScript("loadXML", args);

            //if there is no main function defined, wrap the generated code into main function
            if (lua.IndexOf(fname + "_main(e)", StringComparison.InvariantCultureIgnoreCase) == -1)
            {
                lua = "function " + fname + "_main(e)\n" + lua + "\nend";
            }

            this.scriptName = fname;

            string b64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(xml));
            lua += "\n\n-- VLD: " + b64;

            //MessageBox.Show(filename + "\n\n" + lua);

            File.WriteAllText(filename, lua);
        }

        public void updateTitle()
        {
            string tit = "Visual LUA Designer v.0.8 :: script file: ";

            if (this.fileName.Equals(""))
                tit += "untitled";
            else
                tit += Path.GetFileName(this.fileName);

            this.Text = tit;
        }

        private static DialogResult ShowInputDialog(ref string input, string title)
        {
            System.Drawing.Size size = new System.Drawing.Size(200, 70);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = title;
            inputBox.StartPosition = FormStartPosition.CenterParent;

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = input;
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();

            if (result == DialogResult.OK)
                input = textBox.Text;

            return result;
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //tabControl1.TabPages.Add(new VLDTab());

            NewScriptForm nsf = new NewScriptForm();
            nsf.ShowDialog();
            return;

            String name = "script";
            DialogResult dr = ShowInputDialog(ref name, "Script name");

            if (dr != DialogResult.OK)
                return;

            this.scriptName = name.ToLower();
            this.fileName = "";
            this.updateTitle();

           // webBrowser1.Document.InvokeScript("clearXML");

            name = name.ToLower();
            String[] repl = new String[] { " ", "!", "?", "/", "\\", "|", "#", "$", "@", "%", "^", "&", "*", "(", ")", "[", "]", "{", "}" };
            foreach (String str in repl)
            {
                name = name.Replace(str, "");
            }

            Object[] args = new Object[1];
            //args[0] = this.initXML.Replace("script_init", name + "_init").Replace("script_main", name + "_main");
            args[0] = name;
           // webBrowser1.Document.InvokeScript("loadXML", args);
            webBrowser1.Document.InvokeScript("newScript", args);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "LUA script files (*.lua)|*.lua";
            sfd.InitialDirectory = Program.fpscdir+@"\Files\scriptbank";
            sfd.FileName = this.scriptName;

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                this.fileName = sfd.FileName.ToLower();
                //this.scriptName = Path.GetFileNameWithoutExtension(this.fileName);
                this.saveFile(sfd.FileName.ToLower());
            }

            this.updateTitle();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter = "LUA script files (*.lua)|*.lua";
            ofd.InitialDirectory = Program.fpscdir + @"\Files\scriptbank";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                bool loaded = this.loadFile(ofd.FileName);

                if (loaded == true)
                {
                    this.scriptName = Path.GetFileNameWithoutExtension(ofd.FileName);
                    this.fileName = ofd.FileName;
                }
            }

            this.updateTitle();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Made by Nemanja Avramović & Waldemar Medina © 2014-" + DateTime.Now.Year.ToString());
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("http://vld.fpscr.tk/docs/");
        }

    }
}
