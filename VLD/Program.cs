﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace VLD
{
    static class Program
    {

        public static String path, exedir, fpscdir;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Program.path = Environment.GetCommandLineArgs()[0];
            Program.exedir = System.IO.Path.GetDirectoryName(path);


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(MainForm.getInstance());


        }
    }
}
