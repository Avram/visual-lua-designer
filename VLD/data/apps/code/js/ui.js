//.NET Interaction
//---------------------------------------------------------------------------------------

//Ititial XML: contains the two main functions of the script
var init_xml = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="procedures_defnoreturn" id="74" x="44" y="8"><mutation><arg name="e"></arg></mutation><field name="NAME">{script.name}_init</field></block><block type="procedures_defnoreturn" id="75" x="42" y="79"><mutation><arg name="e"></arg></mutation><field name="NAME">{script.name}_main</field></block></xml>';



newFile = function(){
      if (Blockly.mainWorkspace !== null) {
        confirm("Make sure you have save your project");
        Blockly.mainWorkspace.clear();
        e = prompt("Enter New Script Name", "");
        if (e != null) {
            scriptName = e;
        }
    }
}

openFile = function(fileStream){
    var xml = Blockly.Xml.textToDom(fileStream);
    Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
}

saveProject = function(){
  var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
  var xml_text = Blockly.Xml.domToText(xml);
  return xml_text;

}


var konamiCode = function(combination, callback) {
  var lastCorrectInput = - 1,
  isActive = 0,
  o = {};
  if (typeof combination === "function") {
    callback = combination;
  }
  if (Object.prototype.toString.call(combination) !== "[object Array]") {
    combination = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65];
  }
  o.start = function() {
    if (isActive) {
    return;
  }
  isActive = 1;
  document.onkeyup = function(e) {
   var code;
   if (!isActive) {
    return;
   }
   code = window.event ? window.event.keyCode: e.which;
   if (combination[++lastCorrectInput] === code) {
    if (lastCorrectInput === combination.length - 1) {
     if (callback && typeof(callback) === "function") {
      callback();
     }
    }
    return;
   }
   lastCorrectInput = - 1;
  };
  return o;
 };
 o.stop = function() {
  isActive = 0;
  return o;
 };
 return o;
};

function setCookie(c_name,value,exdays)
{
  var exdate=new Date();
  exdate.setDate(exdate.getDate() + exdays);
  var c_value=escape(value) +
  ((exdays==null) ? "" : ("; expires="+exdate.toUTCString()));
  document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name)
{
  var i,x,y,ARRcookies=document.cookie.split(";");
  for (i=0;i<ARRcookies.length;i++)
  {
    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
    y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
    x=x.replace(/^\s+|\s+$/g,"");
    if (x==c_name)
    {
      return unescape(y);
    }
  }
}


function esteban()
{
  Shadowbox.open({
        content:    SITE_URL+'/misc/esteban',
        player:     "iframe",
        title:      "Eeaster Egg Unlocked!",
        height:     480,
        width:      640,
        modal:      true
    });
}


function init() {
    Blockly.inject(document.getElementById('sb-page'),
      {path: BASE_URL+'/', toolbox: document.getElementById('toolbox')});
    // Let the top-level application know that Blockly is ready.


    //var startwith = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="procedures_defnoreturn" x="207" y="216"><mutation><arg name="e"></arg></mutation><title name="NAME">script_main</title><statement name="STACK"><block type="controls_if" inline="false"><value name="IF0"><block type="logic_compare" inline="true"><title name="OP">LTE</title><value name="A"><block type="vlh_playerdist" inline="true"><value name="ENTITY"><block type="fpsc_entity"></block></value></block></value><value name="B"><block type="math_number"><title name="NUM">50</title></block></value></block></value><statement name="DO0"><block type="fpsc_prompt" inline="true"><value name="TEXT"><block type="text"><title name="TEXT">Hello there!</title></block></value></block></statement></block></statement></block></xml>';
    var startwith = '<xml xmlns="http://www.w3.org/1999/xhtml"></xml>';

    var xml = Blockly.Xml.textToDom(startwith);
    Blockly.Xml.domToWorkspace( Blockly.mainWorkspace, xml );

    //window.parent.blocklyLoaded(Blockly);
}

function xml() {
    var xml = Blockly.Xml.workspaceToDom( Blockly.mainWorkspace );
    var text = Blockly.Xml.domToText( xml );

    $('body').html('<textarea>'+text+'</textarea>');
}

function clearXML()
{
    Blockly.mainWorkspace.clear();    
}



function newScript(scriptName) {
    var newXml;
    scriptName == undefined ? newXml = init_xml.replace(/{script.name}/g, "script") : newXml = init_xml.replace(/{script.name}/g, scriptName);
    
    clearXML();

    var xml = Blockly.Xml.textToDom(newXml);
    Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
}


function loadXML(xml)
{
  var xmld = Blockly.Xml.textToDom(xml);
  Blockly.Xml.domToWorkspace( Blockly.mainWorkspace, xmld );
}

function gimmeXML(scriptname)
{
  var domxml = Blockly.Xml.workspaceToDom( Blockly.mainWorkspace );
  var xml = Blockly.Xml.domToText( domxml );
  var code = Blockly.Lua.workspaceToCode();

  window.external.hereItIs(xml, code, scriptname);
}

