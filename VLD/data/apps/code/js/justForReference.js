var fpsc = {};

var medina = null;

fpsc.setCookie = function(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) +
	((exdays==null) ? "" : ("; expires="+exdate.toUTCString()));
	document.cookie=c_name + "=" + c_value;
}

fpsc.getCookie = function(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==c_name)
			{
			return unescape(y);
			}
		}
}

fpsc.init = function() {
	
    var fpsc.workSpace = Blockly.inject(document.getElementById('workspace'),
      {path: '../../', toolbox: document.getElementById('toolbox')});
    // Let the top-level application know that Blockly is ready.

    var startwith = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="procedures_defnoreturn" x="207" y="216"><mutation><arg name="e"></arg></mutation><title name="NAME">script_main</title><statement name="STACK"><block type="controls_if" inline="false"><value name="IF0"><block type="logic_compare" inline="true"><title name="OP">LTE</title><value name="A"><block type="vlh_playerdist" inline="true"><value name="ENTITY"><block type="fpsc_entity"></block></value></block></value><value name="B"><block type="math_number"><title name="NUM">50</title></block></value></block></value><statement name="DO0"><block type="fpsc_prompt" inline="true"><value name="TEXT"><block type="text"><title name="TEXT">Hello there!</title></block></value></block></statement></block></statement></block></xml>';

    var xml = Blockly.Xml.textToDom(startwith);
    Blockly.Xml.domToWorkspace( Blockly.mainWorkspace, xml );
    //window.parent.blocklyLoaded(Blockly);
	
	//Add Event Listener
	//Blockly.addChangeListener(fpsc.renderCode);
	fpsc.workSpace.addChangeListener(fspc.renderCode);
}

fpsc.renderCode = function() {	
    var code = Blockly.Lua.workspaceToCode();
	content = document.getElementById('codeRenderer');
	content.textContent = code;
	$(content).removeClass("prettyprinted");
	prettyPrint();	
}


fpsc.xml = function() {
    var xml = Blockly.Xml.workspaceToDom( Blockly.mainWorkspace );
    var text = Blockly.Xml.domToText( xml );
    $('body').append('<textarea>'+text+'</textarea>');
}


