
var fpsc = {};

var $classic;
var $reloaded;

fpsc.workspace = null;

fpsc.init = function() {

	$classic = document.getElementById('classic_toolbox');
	$reloaded = document.getElementById('reloaded_core_toolbox');


    if(parent.fpscVer == "c")
    	$toolbar =  $classic;
    else
        $toolbar = $reloaded;


    //Blockly Initialization
   fpsc.workspace =  Blockly.inject(document.body, {
    	path: '../../', 
    	toolbox: $toolbar, 
    	scrollbars: true, 
    	trashcan: true
    });
    
    // Let the top-level application know that Blockly is ready.
    window.parent.blocklyLoaded(Blockly);

    //Load Default Blocks on the workspace
    window.parent.newScript();
}

fpsc.newScript = function(){
    var xml = Blockly.Xml.textToDom(init_xml);
    Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
}


fpsc.loadVer = function(ver){
	if(ver=='c'){
		parent.fpscVer = ver;
		fpsc.workspace.updateToolbox($classic);
	}
	else if (ver=='r'){
		parent.fpscVer = ver;
		fpsc.workspace.updateToolbox($reloaded);
	}
}

fpsc.loadToolBox = function(toolbox){
	fpsc.workspace.updateToolbox(document.getElementById(toolbox));
}

fpsc.NewScript = function(toolbox){}
