//GLOBAL GAME GURU VARIABLES

var helpURL = "http://vld.fpscr.tk/docs/core/",
	functionsColor = 230,
	globalsColor = 260;

// AI_CONSTANTS

Blockly.Blocks['fpsc_ai_manual'] = {
  init: function() {
    this.setHelpUrl(helpURL+"ai_manual");
    this.setColour(globalsColor);
		this.appendDummyInput().appendField('ai_manual');
		this.appendValueInput("VALUE").setCheck(null);
		this.setInputsInline(!0);
		this.setPreviousStatement(!0);
		this.setNextStatement(!0);
    this.setTooltip("AI MANUAL 0");
  }
}; 
  
Blockly.Lua['fpsc_ai_manual'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'AI_MANUAL = '+argument+"\n";
  return code;
}; 

Blockly.Blocks['fpsc_get_ai_manual'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('AI_MANUAL', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current (int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_manual'] = function(block) {
  return ['AI_MANUAL', Blockly.Lua.ORDER_UNARY];
};

Blockly.Blocks['fpsc_ai_automatic'] = {
  init: function() {
    this.setHelpUrl(helpURL+"ai_automatic");
    this.setColour(globalsColor);
		this.appendDummyInput().appendField('ai_automatic');
		this.appendValueInput("VALUE").setCheck(null);
		this.setInputsInline(!0);
		this.setPreviousStatement(!0);
		this.setNextStatement(!0);
    this.setTooltip("AI_AUTOMATIC (integer)");
  }
}; 
  
Blockly.Lua['fpsc_ai_automatic'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'AI_AUTOMATIC = '+argument+"\n";
  return code;
};  


Blockly.Blocks['fpsc_get_ai_automatic'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('AI_AUTOMATIC', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_automatic(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_automatic'] = function(block) {
  return ['AI_AUTOMATIC', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_closest_to_player'] = {
  init: function() {
    this.setHelpUrl(helpURL+"ai_closest_to_player");
    this.setColour(globalsColor);
		this.appendDummyInput().appendField('ai_closest_to_player');
		this.appendValueInput("VALUE").setCheck(null);
		this.setInputsInline(!0);
		this.setPreviousStatement(!0);
		this.setNextStatement(!0);
    this.setTooltip("AI_CLOSEST_TO_PLAYER (int)");
  }
}; 
  
Blockly.Lua['fpsc_ai_closest_to_player'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'AI_CLOSEST_TO_PLAYER = '+argument+"\n";
  return code;
}; 

Blockly.Blocks['fpsc_get_ai_closest_to_player'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('AI_CLOSEST_TO_PLAYER', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_closest_to_player (int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_closest_to_player'] = function(block) {
  return ['AI_CLOSEST_TO_PLAYER', Blockly.Lua.ORDER_UNARY];
};

// -- Globals (built-in)
Blockly.Blocks['fpsc_g_Entity'] = {
  init: function() {
    this.setHelpUrl(helpURL+"g_Entity");
    this.setColour(260);
		this.appendDummyInput().appendField('g_Entity');
		this.appendValueInput("VALUE").setCheck(null);
		this.setInputsInline(!0);
		this.setPreviousStatement(!0);
		this.setNextStatement(!0);
    this.setTooltip("Global entities list g_Entity (array)");
  }
}; 
  
Blockly.Lua['fpsc_g_Entity'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_Entity = '+argument+"\n";
  return code;
};  

Blockly.Blocks['fpsc_get_g_Entity'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_Entity', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_Entity(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_Entity'] = function(block) {
  return ['g_Entity', Blockly.Lua.ORDER_UNARY];
};

// -- AI Globals
Blockly.Blocks['fpsc_ai_soldier_state'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_soldier_state");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_soldier_state');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_soldier_state (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_soldier_state'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_soldier_state = '+argument+"\n";
  return code;
}; 
Blockly.Blocks['fpsc_get_ai_soldier_state'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_soldier_state', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_soldier_state(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_soldier_state'] = function(block) {
  return ['ai_soldier_state', Blockly.Lua.ORDER_UNARY];
};

Blockly.Blocks['fpsc_ai_soldier_pathindex'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_soldier_pathindex");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_soldier_pathindex');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_soldier_pathindex (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_soldier_pathindex'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_soldier_pathindex = '+argument+"\n";
  return code;
};  
Blockly.Blocks['fpsc_get_ai_soldier_pathindex'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_soldier_pathindex', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current (int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_soldier_pathindex'] = function(block) {
  return ['ai_soldier_pathindex', Blockly.Lua.ORDER_UNARY];
};

Blockly.Blocks['fpsc_ai_combat_mode'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_combat_mode");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_combat_mode');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_combat_mode (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_combat_mode'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_combat_mode = '+argument+"\n";
  return code;
};  
Blockly.Blocks['fpsc_get_ai_combat_mode'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_combat_mode', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_combat_mode(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_combat_mode'] = function(block) {
  return ['ai_combat_mode', Blockly.Lua.ORDER_UNARY];
};

Blockly.Blocks['fpsc_ai_combat_state_delay'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_combat_state_delay");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_combat_state_delay');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_combat_state_delay (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_combat_state_delay'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_combat_state_delay = '+argument+"\n";
  return code;
};

Blockly.Blocks['fpsc_get_ai_combat_state_delay'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_combat_state_delay', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current (int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_combat_state_delay'] = function(block) {
  return ['ai_combat_state_delay', Blockly.Lua.ORDER_UNARY];
};

Blockly.Blocks['fpsc_ai_combat_old_time'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_combat_old_time");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_combat_old_time');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_combat_old_time (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_combat_old_time'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_combat_old_time = '+argument+"\n";
  return code;
};

Blockly.Blocks['fpsc_get_ai_combat_old_time'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_combat_old_time', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_combat_old_time(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_combat_old_time'] = function(block) {
  return ['ai_combat_old_time', Blockly.Lua.ORDER_UNARY];
};

Blockly.Blocks['fpsc_ai_combat_turn_delay'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_combat_turn_delay");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_combat_turn_delay');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_combat_turn_delay (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_combat_turn_delay'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_combat_turn_delay = '+argument+"\n";
  return code;
};

Blockly.Blocks['fpsc_get_ai_combat_turn_delay'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_combat_turn_delay', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_combat_turn_delay(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_combat_turn_delay'] = function(block) {
  return ['ai_combat_turn_delay', Blockly.Lua.ORDER_UNARY];
};

Blockly.Blocks['fpsc_ai_combat_cover_delay'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_combat_cover_delay");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_combat_cover_delay');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_combat_cover_delay (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_combat_cover_delay'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_combat_cover_delay = '+argument+"\n";
  return code;
};

Blockly.Blocks['fpsc_get_ai_combat_cover_delay'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_combat_cover_delay', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_combat_cover_delay(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_combat_cover_delay'] = function(block) {
  return ['ai_combat_cover_delay', Blockly.Lua.ORDER_UNARY];
};

Blockly.Blocks['fpsc_ai_combat_delay_after_finding'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_combat_delay_after_finding");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_combat_delay_after_finding');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_combat_delay_after_finding (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_combat_delay_after_finding'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_combat_delay_after_finding = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_combat_delay_after_finding'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_combat_delay_after_finding', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_combat_delay_after_finding(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_combat_delay_after_finding'] = function(block) {
  return ['ai_combat_delay_after_finding', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_next_aggro_delay'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_next_aggro_delay");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_next_aggro_delay');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_next_aggro_delay (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_next_aggro_delay'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_next_aggro_delay = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_next_aggro_delay'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_next_aggro_delay', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current (int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_next_aggro_delay'] = function(block) {
  return ['e', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_start_x'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_start_x");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_start_x');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_start_x (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_start_x'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_start_x = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_start_x'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_start_x', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_start_x(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_start_x'] = function(block) {
  return ['ai_start_x', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_start_z'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_start_z");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_start_z');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_start_z (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_start_z'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_start_z = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_start_z'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_start_z', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_start_z(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_start_z'] = function(block) {
  return ['ai_start_z', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_dest_x'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_dest_x");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_dest_x');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_dest_x (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_dest_x'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_dest_x = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_dest_x'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_dest_x', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_dest_x(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_dest_x'] = function(block) {
  return ['ai_dest_x', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_dest_z'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_dest_z");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_dest_z');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_dest_z (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_dest_z'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_dest_z = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_dest_z'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_dest_z', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_dest_z(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_dest_z'] = function(block) {
  return ['ai_dest_z', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_old_health'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_old_health");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_old_health');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_old_health (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_old_health'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_old_health = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_old_health'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_old_health', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_old_health(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_old_health'] = function(block) {
  return ['ai_old_health', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_ran_to_cover'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_ran_to_cover");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_ran_to_cover');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_ran_to_cover (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_ran_to_cover'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_ran_to_cover = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_ran_to_cover'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_ran_to_cover', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_ran_to_cover(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_ran_to_cover'] = function(block) {
  return ['ai_ran_to_cover', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_alerted_mode'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_alerted_mode");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_alerted_mode');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_alerted_mode (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_alerted_mode'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_alerted_mode = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_alerted_mode'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_alerted_mode', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_alerted_mode(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_alerted_mode'] = function(block) {
  return ['ai_alerted_mode', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_alerted_state_delay'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_alerted_state_delay");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_alerted_state_delay');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_alerted_state_delay (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_alerted_state_delay'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_alerted_state_delay = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_alerted_state_delay'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_alerted_state_delay', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_alerted_state_delay(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_alerted_state_delay'] = function(block) {
  return ['ai_alerted_state_delay', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_alerted_old_time'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_alerted_old_time");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_alerted_old_time');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_alerted_old_time (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_alerted_old_time'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_alerted_old_time = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_alerted_old_time'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_alerted_old_time', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_alerted_old_time(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_alerted_old_time'] = function(block) {
  return ['ai_alerted_old_time', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_alerted_spoken'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_alerted_spoken");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_alerted_spoken');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_alerted_spoken (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_alerted_spoken'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_alerted_spoken = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_alerted_spoken'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_alerted_spoken', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_alerted_spoken(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_alerted_spoken'] = function(block) {
  return ['ai_alerted_spoken', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_returning_home'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_returning_home");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_returning_home');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_returning_home (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_returning_home'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_returning_home = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_returning_home'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_returning_home', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_returning_home(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_returning_home'] = function(block) {
  return ['ai_returning_home', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_alert_x'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_alert_x");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_alert_x');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_alert_x (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_alert_x'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_alert_x = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_alert_x'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_alert_x', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_alert_x(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_alert_x'] = function(block) {
  return ['ai_alert_x', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_alert_z'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_alert_z");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_alert_z');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_alert_z (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_alert_z'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_alert_z = '+argument+"\n";
  return code;
};



Blockly.Blocks['fpsc_get_ai_alert_z'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_alert_z', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_alert_z(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_alert_z'] = function(block) {
  return ['ai_alert_z', Blockly.Lua.ORDER_UNARY];
};

	  

Blockly.Blocks['fpsc_ai_alert_counter'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_alert_counter");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_alert_counter');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_alert_counter (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_alert_counter'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_alert_counter = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_alert_counter'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_alert_counter', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_alert_counter(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_alert_counter'] = function(block) {
  return ['ai_alert_counter', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_alert_entity'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_alert_entity");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_alert_entity');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_ (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_alert_entity'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_alert_entity = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_alert_entity'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_alert_entity', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_alert_entity(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_alert_entity'] = function(block) {
  return ['ai_alert_entity', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_path_point_index'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_path_point_index");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_path_point_index');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_path_point_index (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_path_point_index'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_path_point_index = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_path_point_index'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_path_point_index(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_path_point_index'] = function(block) {
  return ['ai_path_point_index', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_path_point_direction'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_path_point_direction");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_path_point_direction');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_path_point_direction (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_path_point_direction'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_path_point_direction = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_path_point_direction'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_path_point_direction', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_path_point_direction(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_path_point_direction'] = function(block) {
  return ['ai_path_point_direction', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_path_point_max'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_path_point_max");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_path_point_max');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_path_point_max (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_path_point_max'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_path_point_max = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_path_point_max'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_path_point_max', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_point_max(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_path_point_max'] = function(block) {
  return ['ai_path_point_max', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_starting_heath'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_starting_heath");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_starting_heath');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_starting_heath (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_starting_heath'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_starting_heath = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_starting_heath'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_starting_heath', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_starting_heath(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_starting_heath'] = function(block) {
  return ['ai_starting_heath', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_patrol_x'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_patrol_x");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_patrol_x');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_patrol_x (array)");
	}
}; 

Blockly.Lua['fpsc_ai_patrol_x'] = function(block) {	  
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_patrol_x = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_patrol_x'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_patrol_x', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_patrol_x(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_patrol_x'] = function(block) {
  return ['ai_patrol_x', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_patrol_z'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_patrol_z");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_patrol_z');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_patrol_z (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_patrol_z'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_patrol_z = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_patrol_z'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_patrol_z(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_patrol_z'] = function(block) {
  return ['ai_patrol_z', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_aggro_x'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_aggro_x");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_aggro_x');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_aggro_x (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_aggro_x'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_aggro_x = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_aggro_x'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_aggro_x', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_aggro_x(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_aggro_x'] = function(block) {
  return ['ai_aggro_x', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_aggro_z'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_aggro_z");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_aggro_z');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_aggro_z (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_aggro_z'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_aggro_z = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_aggro_z'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_aggro_z', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_aggro_z(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_aggro_z'] = function(block) {
  return ['ai_aggro_z', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_ai_aggro_entity'] = {
	init: function() {
	this.setHelpUrl(helpURL+"ai_aggro_entity");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('ai_aggro_entity');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ai_aggro_entity (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_aggro_entity'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_aggro_entity = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_ai_aggro_entity'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_aggro_entity', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_aggro_entity(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_aggro_entity'] = function(block) {
  return ['ai_aggro_entity', Blockly.Lua.ORDER_UNARY];
};




Blockly.Blocks['fpsc_ai_aggro_range'] = {
	init: function() {
		this.setHelpUrl(helpURL+"ai_aggro_range");
		this.setColour(globalsColor);
		this.appendDummyInput().appendField('ai_aggro_range');
		this.appendValueInput("VALUE").setCheck(null);
		this.setInputsInline(!0);
		this.setPreviousStatement(!0);
		this.setNextStatement(!0);
		this.setTooltip("Global entities list ai_aggro_range (array)");
	}
}; 
	  
Blockly.Lua['fpsc_ai_aggro_range'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'ai_aggro_range = '+argument+"\n";
  return code;
};



Blockly.Blocks['fpsc_get_ai_aggro_range'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('ai_aggro_range', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ai_aggro_range(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_ai_aggro_range'] = function(block) {
  return ['ai_aggro_range', Blockly.Lua.ORDER_UNARY];
};



Blockly.Blocks['fpsc_PlayerDist'] = {   /// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	init: function() {
		this.setHelpUrl(helpURL+"PlayerDist");
		this.setColour(globalsColor);
		this.appendDummyInput().appendField('PlayerDist');
		this.appendValueInput("VALUE").setCheck(null);
		this.setInputsInline(!0);
		this.setPreviousStatement(!0);
		this.setNextStatement(!0);
		this.setTooltip("Algun Mensaje Aqui: Variable Global PlayerDist")
	}
};
 

Blockly.Lua['fpsc_PlayerDist'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'PlayerDist = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_PlayerDist'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('PlayerDist', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current PlayerDist(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_PlayerDist'] = function(block) {
  return ['PlayerDist', Blockly.Lua.ORDER_UNARY];
};


//  -- Weapon name global

Blockly.Blocks['fpsc_weapon_name'] = {
	init: function() {
	this.setHelpUrl(helpURL+"weapon_name");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('weapon_name');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list aweapon_name (array)");
	}
}; 
	  
Blockly.Lua['fpsc_weapon_name'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'weapon_name = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_weapon_name'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('weapon_name', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current weapon_name(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_weapon_name'] = function(block) {
  return ['weapon_name', Blockly.Lua.ORDER_UNARY];
};


//  -- Globals (passed-in)

Blockly.Blocks['fpsc_g_PlayerPosX'] = {
  init: function() {
    this.setHelpUrl(helpURL+"g_PlayerPosX");
    this.setColour(functionsColor);
	this.appendDummyInput().appendField('g_PlayerPosX');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
    this.setTooltip("Player X position (int)");
  }
}; 

Blockly.Lua['fpsc_g_PlayerPosX'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerPosX = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerPosX'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_PlayerPosX', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_PlayerPosX(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerPosX'] = function(block) {
  return ['g_PlayerPosX', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_PlayerPosY'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_PlayerPosY");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_PlayerPosY');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_PlayerPosY (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_PlayerPosY'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerPosY = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerPosY'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_PlayerPosY', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_PlayerPosY(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerPosY'] = function(block) {
  return ['g_PlayerPosY', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_PlayerPosZ'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_PlayerPosZ");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_PlayerPosZ');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_PlayerPosZ (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_PlayerPosZ'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerPosZ = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerPosZ'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_PlayerPosZ', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_PlayerPosZ(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerPosZ'] = function(block) {
  return ['g_PlayerPosZ', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_PlayerAngX'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_PlayerAngX");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_PlayerAngX');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_PlayerAngX (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_PlayerAngX'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerAngX = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerAngX'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_PlayerAngX', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_PlayerAngX(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerAngX'] = function(block) {
  return ['g_PlayerAngX', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_PlayerAngY'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_PlayerAngY");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_PlayerAngY');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_PlayerAngY (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_PlayerAngY'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerAngY = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerAngY'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_PlayerAngY', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_PlayerAngY(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerAngY'] = function(block) {
  return ['g_PlayerAngY', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_PlayerAngZ'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_PlayerAngZ");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_PlayerAngZ');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_PlayerAngZ (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_PlayerAngZ'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerAngZ = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerAngZ'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_PlayerAngZ', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_PlayerAngZ(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerAngZ'] = function(block) {
  return ['g_PlayerAngZ', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_PlayerObjNo'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_PlayerObjNo");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_PlayerObjNo');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_PlayerObjNo (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_PlayerObjNo'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerObjNo = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerObjNo'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current (int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerObjNo'] = function(block) {
  return ['g_PlayerObjNo', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_PlayerHealth'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_PlayerHealth");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_PlayerHealth');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_PlayerHealth (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_PlayerHealth'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerHealth = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerHealth'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current (int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerHealth'] = function(block) {
  return ['g_PlayerHealth', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_PlayerLives'] = {
  init: function() {
    this.setHelpUrl(helpURL+"g_PlayerLives");
    this.setColour(260);
	this.appendDummyInput().appendField('g_PlayerLives');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
    this.setTooltip("can read the current number of player lives");
  }
}; 
  
Blockly.Lua['fpsc_g_PlayerLives'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerLives = '+argument+"\n";
  return code;
}; 


Blockly.Blocks['fpsc_get_g_PlayerLives'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_PlayerLives', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_PlayerLives(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerLives'] = function(block) {
  return ['g_PlayerLives', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_PlayerFlashlight'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_PlayerFlashlight");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_PlayerFlashlight');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_PlayerFlashlight (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_g_PlayerFlashlight'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerFlashlight = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerFlashlight'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_PlayerFlashlight', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_PlayerFlashlight(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerFlashlight'] = function(block) {
  return ['g_PlayerFlashlight', Blockly.Lua.ORDER_UNARY];
};



Blockly.Blocks['fpsc_g_PlayerGunCount'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_PlayerGunCount");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_PlayerGunCount');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list ag_PlayerGunCount (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_PlayerGunCount'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerGunCount = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerGunCount'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_PlayerGunCount', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current (int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerGunCount'] = function(block) {
  return ['g_PlayerGunCount', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_PlayerGunID'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_PlayerGunID");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_PlayerGunID');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_PlayerGunID (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_PlayerGunID'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_PlayerGunID = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_PlayerGunID'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_PlayerGunID', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_PlayerGunID(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_PlayerGunID'] = function(block) {
  return ['g_PlayerGunID', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_Time'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_Time");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_Time');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_Time (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_Time'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_Time = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_Time'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_Time', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_Time(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_Time'] = function(block) {
  return ['g_Time', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_TimeElapsed'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_TimeElapsed");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_TimeElapsed');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_TimeElapsed (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_TimeElapsed'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_TimeElapsed = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_TimeElapsed'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_TimeElapsed', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_TimeElapsed(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_TimeElapsed'] = function(block) {
  return ['g_TimeElapsed', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_KeyPressE'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_KeyPressE");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_KeyPressE');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_KeyPressE (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_KeyPressE'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_KeyPressE = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_KeyPressE'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_KeyPressE', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_KeyPressE(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_KeyPressE'] = function(block) {
  return ['g_KeyPressE', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_Scancode'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_Scancode");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_Scancode');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_Scancode (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_Scancode'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_Scancode = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_Scancode'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_Scancode', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current (int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_Scancode'] = function(block) {
  return ['g_Scancode', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_g_InKey'] = {
	init: function() {
	this.setHelpUrl(helpURL+"g_InKey");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('g_InKey');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list g_InKey (array)");
	}
}; 
	  
Blockly.Lua['fpsc_g_InKey'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'g_InKey = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_g_InKey'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('g_InKey', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current g_InKey(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_g_InKey'] = function(block) {
  return ['g_InKey', Blockly.Lua.ORDER_UNARY];
};


//-- Multiplayer

Blockly.Blocks['fpsc_mp_isServer'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_isServer");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_isServer');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list amp_isServer (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_isServer'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_isServer = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_isServer'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_isServer', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_isServer(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_isServer'] = function(block) {
  return ['mp_isServer', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_playerNames'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_playerNames");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_playerNames');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_playerNames (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_playerNames'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_playerNames = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_playerNames'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_playerNames', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_playerNames(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_playerNames'] = function(block) {
  return ['mp_playerNames', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_playerKills'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_playerKills");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_playerKills');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_playerKills (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_playerKills'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_playerKills = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_playerKills'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_playerKills', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_playerKills(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_playerKills'] = function(block) {
  return ['mp_playerKills', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_playerDeaths'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_playerDeaths");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_playerDeaths');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_playerDeaths (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_playerDeaths'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_playerDeaths = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_playerDeaths'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_playerDeaths', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_playerDeaths(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_playerDeaths'] = function(block) {
  return ['mp_playerDeaths', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_playerConnected'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_playerConnected");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_playerConnected');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_playerConnected (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_playerConnected'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_playerConnected = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_playerConnected'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_playerConnected', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current (int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_playerConnected'] = function(block) {
  return ['e', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_playerTeam'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_playerTeam");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_playerTeam');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_playerTeam (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_playerTeam'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_playerTeam = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_playerTeam'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_playerTeam', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_playerTeam(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_playerTeam'] = function(block) {
  return ['mp_playerTeam', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_isConnectedToSteam'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_isConnectedToSteam");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_isConnectedToSteam');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_isConnectedToSteam (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_isConnectedToSteam'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_isConnectedToSteam = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_isConnectedToSteam'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_isConnectedToSteam', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_isConnectedToSteam(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_isConnectedToSteam'] = function(block) {
  return ['mp_isConnectedToSteam', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_gameMode'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_gameMode");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_gameMode');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list amp_gameMode (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_gameMode'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_gameMode = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_gameMode'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_gameMode', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_gameMode(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_gameMode'] = function(block) {
  return ['mp_gameMode', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_servertimer'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_servertimer");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_servertimer');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_servertimer (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_servertimer'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_servertimer = '+argument+"\n";
    return code;
};


Blockly.Blocks['fpsc_get_mp_servertimer'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_servertimer', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_servertimer(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_servertimer'] = function(block) {
  return ['mp_servertimer', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_showscores'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_showscores");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_showscores');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_showscores (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_showscores'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_showscores = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_showscores'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_showscores', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_showscores(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_showscores'] = function(block) {
  return ['e', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_teambased'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_teambased");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_teambased');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_teambased (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_teambased'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_teambased = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_teambased'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_teambased', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_teambased(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_teambased'] = function(block) {
  return ['mp_teambased', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_friendlyfireoff'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_friendlyfireoff");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_friendlyfireoff');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_friendlyfireoff (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_friendlyfireoff'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_friendlyfireoff = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_friendlyfireoff'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_friendlyfireoff', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_friendlyfireoff(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_friendlyfireoff'] = function(block) {
  return ['mp_friendlyfireoff', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_me'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_me");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_me');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_me (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_me'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_me = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_me'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_me', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_me(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_me'] = function(block) {
  return ['mp_me', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_coop'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_coop");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_coop');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_coop (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_coop'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_coop = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_coop'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_coop', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_coop(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_coop'] = function(block) {
  return ['mp_coop', Blockly.Lua.ORDER_UNARY];
};


Blockly.Blocks['fpsc_mp_enemiesLeftToKill'] = {
	init: function() {
	this.setHelpUrl(helpURL+"mp_enemiesLeftToKill");
	this.setColour(globalsColor);
	this.appendDummyInput().appendField('mp_enemiesLeftToKill');
	this.appendValueInput("VALUE").setCheck(null);
	this.setInputsInline(!0);
	this.setPreviousStatement(!0);
	this.setNextStatement(!0);
	this.setTooltip("Global entities list mp_enemiesLeftToKill (array)");
	}
}; 
	  
Blockly.Lua['fpsc_mp_enemiesLeftToKill'] = function(block) {
  	argument =  Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_RELATIONAL) || '""',
    code = 'mp_enemiesLeftToKill = '+argument+"\n";
  return code;
};


Blockly.Blocks['fpsc_get_mp_enemiesLeftToKill'] = {  
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('mp_enemiesLeftToKill', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current mp_enemiesLeftToKill(int)');
  }
}; 
  
Blockly.Lua['fpsc_get_mp_enemiesLeftToKill'] = function(block) {
  return ['mp_enemiesLeftToKill', Blockly.Lua.ORDER_UNARY];
};