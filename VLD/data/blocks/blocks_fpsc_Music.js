Blockly.Blocks['fpsc_MUSIC_TRACK1'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK1");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK1', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK1");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK1'] = function(block) {
  return ['MUSIC_TRACK1', Blockly.Lua.ORDER_UNARY];
}; 

Blockly.Blocks['fpsc_MUSIC_TRACK1_INTERVAL'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK1_INTERVAL");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK1_INTERVAL', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK1_INTERVAL");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK1_INTERVAL'] = function(block) {
  return ['MUSIC_TRACK1_INTERVAL', Blockly.Lua.ORDER_UNARY];
}; 


Blockly.Blocks['fpsc_MUSIC_TRACK1_LENGTH'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK1_LENGTH");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK1_LENGTH', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK1_LENGTH");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK1_LENGTH'] = function(block) {
  return ['MUSIC_TRACK1_LENGTH', Blockly.Lua.ORDER_UNARY];
}; 



Blockly.Blocks['fpsc_MUSIC_TRACK2'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK2");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK2', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK2");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK2'] = function(block) {
  return ['MUSIC_TRACK2', Blockly.Lua.ORDER_UNARY];
}; 

Blockly.Blocks['fpsc_MUSIC_TRACK2_INTERVAL'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK2_INTERVAL");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK2_INTERVAL', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK2_INTERVAL");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK2_INTERVAL'] = function(block) {
  return ['MUSIC_TRACK2_INTERVAL', Blockly.Lua.ORDER_UNARY];
}; 


Blockly.Blocks['fpsc_MUSIC_TRACK2_LENGTH'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK2_LENGTH");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK2_LENGTH', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK2_LENGTH");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK2_LENGTH'] = function(block) {
  return ['MUSIC_TRACK2_LENGTH', Blockly.Lua.ORDER_UNARY];
}; 


Blockly.Blocks['fpsc_MUSIC_TRACK3'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK3");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK3', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK3");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK3'] = function(block) {
  return ['MUSIC_TRACK3', Blockly.Lua.ORDER_UNARY];
}; 

Blockly.Blocks['fpsc_MUSIC_TRACK3_INTERVAL'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK3_INTERVAL");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK3_INTERVAL', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK3_INTERVAL");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK3_INTERVAL'] = function(block) {
  return ['MUSIC_TRACK3_INTERVAL', Blockly.Lua.ORDER_UNARY];
}; 


Blockly.Blocks['fpsc_MUSIC_TRACK3_LENGTH'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK3_LENGTH");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK3_LENGTH', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK3_LENGTH");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK3_LENGTH'] = function(block) {
  return ['MUSIC_TRACK3_LENGTH', Blockly.Lua.ORDER_UNARY];
}; 


Blockly.Blocks['fpsc_MUSIC_TRACK4'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK4");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK4', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK4");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK14'] = function(block) {
  return ['MUSIC_TRACK1', Blockly.Lua.ORDER_UNARY];
}; 

Blockly.Blocks['fpsc_MUSIC_TRACK4_INTERVAL'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK4_INTERVAL");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK4_INTERVAL', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK4_INTERVAL");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK4_INTERVAL'] = function(block) {
  return ['MUSIC_TRACK4_INTERVAL', Blockly.Lua.ORDER_UNARY];
}; 


Blockly.Blocks['fpsc_MUSIC_TRACK4_LENGTH'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK4_LENGTH");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK4_LENGTH', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK4_LENGTH");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK4_LENGTH'] = function(block) {
  return ['MUSIC_TRACK4_LENGTH', Blockly.Lua.ORDER_UNARY];
}; 


Blockly.Blocks['fpsc_MUSIC_TRACK_DEFAULT'] = {
  init: function() {
    this.setHelpUrl(helpURL+"MUSIC_TRACK_DEFAULT");
    this.setColour(globalsColor);
    this.interpolateMsg('MUSIC_TRACK_DEFAULT', ['List', 'Array', Blockly.ALIGN_RIGHT ], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Array');
    this.setTooltip("MUSIC_TRACK_DEFAULT");
  }
}; 
  
Blockly.Lua['fpsc_MUSIC_TRACK_DEFAULT'] = function(block) {
  return ['MUSIC_TRACK_DEFAULT', Blockly.Lua.ORDER_UNARY];
}; 



Blockly.Blocks['fpsc_music_init'] = {
	init: function() {
		this.setHelpUrl(helpURL+"music_init");
		this.setColour(290);
		this.interpolateMsg('music_init', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("init when level first runs, load our music and play the default track");
	}
};

Blockly.Lua['fpsc_music_init'] = function(block) {
  var code = 'music_init()' +"\n";
  return code;
};


Blockly.Blocks['fpsc_music_play'] = {
	init: function() {
		this.setHelpUrl(helpURL+"music_play");
		this.setColour(290);
		this.interpolateMsg('music_play', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('MUSIC').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('FADETIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);					
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('plays m fading in over fadetime, stopping any other music over fadetime * 3');
	}

};

Blockly.Lua['fpsc_music_play'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'MUSIC', Blockly.Lua.ORDER_NONE) || 'm';
	argument[1] = Blockly.Lua.valueToCode(block, 'FADETIME', Blockly.Lua.ORDER_NONE) || 'fadeTime';
  var code = 'music_play(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_music_play_instant'] = {
	init: function() {
		this.setHelpUrl(helpURL+"music_play_instant");
		this.setColour(290);
		this.interpolateMsg('music_play', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('MUSIC').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('FADETIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);					
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('plays m fading in over fadetime, stopping any other music instantly');
	}

};

Blockly.Lua['fpsc_music_play_instant'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'MUSIC', Blockly.Lua.ORDER_NONE) || 'm';
	argument[1] = Blockly.Lua.valueToCode(block, 'FADETIME', Blockly.Lua.ORDER_NONE) || 'fadeTime';
  var code = 'music_play_instant(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_music_play_cue'] = {
	init: function() {
		this.setHelpUrl(helpURL+"music_play_instant");
		this.setColour(290);
		this.interpolateMsg('music_play_cue', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('MUSIC').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('FADETIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);					
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('plays m at the next interval of the current music, starting at full volume and fading out the current music by fadeTime');
	}

};

Blockly.Lua['fpsc_music_play_cue'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'MUSIC', Blockly.Lua.ORDER_NONE) || 'm';
	argument[1] = Blockly.Lua.valueToCode(block, 'FADETIME', Blockly.Lua.ORDER_NONE) || 'fadeTime';
  var code = 'music_play_cue(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_music_play_time'] = {
	init: function() {
		this.setHelpUrl(helpURL+"music_play_time");
		this.setColour(290);
		this.interpolateMsg('music_play_time', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('MUSIC').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('PLAYTIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);		
		this.appendValueInput('FADETIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);					
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('as music_play, but plays m for time playTime before returning to play the default track');
	}
};

Blockly.Lua['fpsc_music_play_time'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'MUSIC', Blockly.Lua.ORDER_NONE) || 'm';
	argument[1] = Blockly.Lua.valueToCode(block, 'PLAYTIME', Blockly.Lua.ORDER_NONE) || 'm';
	argument[2] = Blockly.Lua.valueToCode(block, 'FADETIME', Blockly.Lua.ORDER_NONE) || 'fadeTime';
  var code = 'music_play_time(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_music_play_timecue'] = {
	init: function() {
		this.setHelpUrl(helpURL+"music_play_timecue");
		this.setColour(290);
		this.interpolateMsg('music_play_timecue', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('MUSIC').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('PLAYTIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);		
		this.appendValueInput('FADETIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);					
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('as music_play_cue, but plays m for time playTime before returning to play the default track, using timing intervals');
	}
};

Blockly.Lua['fpsc_music_play_timecue'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'MUSIC', Blockly.Lua.ORDER_NONE) || 'm';
	argument[1] = Blockly.Lua.valueToCode(block, 'PLAYTIME', Blockly.Lua.ORDER_NONE) || 'm';
	argument[2] = Blockly.Lua.valueToCode(block, 'FADETIME', Blockly.Lua.ORDER_NONE) || 'fadeTime';
  var code = 'music_play_timecue(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_music_stop'] = {
	init: function() {
		this.setHelpUrl(helpURL+"music_stop");
		this.setColour(290);
		this.interpolateMsg('music_stop', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('FADETIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);					
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('stops the music playing in time fadeTime');
	}

};

Blockly.Lua['fpsc_music_stop'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'FADETIME', Blockly.Lua.ORDER_NONE) || 'fadeTime';
    var code = 'music_stop(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_music_set_volume'] = {
	init: function() {
		this.setHelpUrl(helpURL+"");
		this.setColour(290);
		this.interpolateMsg('music_set_volume', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('FADETIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);					
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('');
	}

};

Blockly.Lua['fpsc_music_set_volume'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'v';
	argument[1] = Blockly.Lua.valueToCode(block, 'FADETIME', Blockly.Lua.ORDER_NONE) || 'fadeTime';
  var code = 'music_set_volume(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_music_set_default'] = {
	init: function() {
		this.setHelpUrl(helpURL+"music_set_default");
		this.setColour(290);
		this.interpolateMsg('music_set_default', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('MUSIC').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('sets the default music track m (the track returned to after another track is played for a finite period of time)');
	}

};

Blockly.Lua['fpsc_music_set_default'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'MUSIC', Blockly.Lua.ORDER_NONE) || 'm';
  var code = 'music_set_default(' + argument.join(',') + ')\n';
  return code;
};



Blockly.Blocks['fpsc_music_set_fadetime'] = {
	init: function() {
		this.setHelpUrl(helpURL+"music_set_fadetime");
		this.setColour(290);
		this.interpolateMsg('music_set_fadetime', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('FADETIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('sets the global fadetime for all subsequent commands that use fade');
	}

};

Blockly.Lua['fpsc_music_set_fadetime'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'FADETIME', Blockly.Lua.ORDER_NONE) || 'e';
  var code = 'music_set_fadetime(' + argument.join(',') + ')\n';
  return code;
};