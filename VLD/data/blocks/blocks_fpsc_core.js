/* 
   	HACKINC2000 & AVRAM BLOCKS
   	UPDATED: 6/9/2015
   	GAME GURU V1.01.001
 */

//-- Common Updater Functions (called by Engine)

Blockly.Blocks['fpsc_UpdateEntitySMALL'] = {
	init: function() {
		this.setHelpUrl(helpURL+"UpdateEntitySMALL");
		this.setColour(functionsColor);
		this.interpolateMsg('UpdateEntitySMALL', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('OBJECT').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('Z').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("");
	}
};

Blockly.Lua['fpsc_UpdateEntitySMALL'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  	argument[1] = Blockly.Lua.valueToCode(block, 'OBJECT', Blockly.Lua.ORDER_NONE) || 'object';  	
  	argument[2] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
  	argument[3] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';
  	argument[4] = Blockly.Lua.valueToCode(block, 'Z', Blockly.Lua.ORDER_NONE) || 'z';
  	var code = 'UpdateEntitySMALL(' + argument.join(',') + ')\n';
  	return code;
};


Blockly.Blocks['fpsc_UpdateEntity'] = {
	init: function() {
		this.setHelpUrl(helpURL+"UpdateEntity");
		this.setColour(functionsColor);
		this.interpolateMsg('UpdateEntity', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('OBJECT').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('Z').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('act').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('col').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('key').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('zon').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('plrvis').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('ani').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('hea').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('frm').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('tmr').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('pdst').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('avd').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("UpdateEntity");
	}
};

Blockly.Lua['fpsc_UpdateEntity'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  	argument[1] = Blockly.Lua.valueToCode(block, 'OBJECT', Blockly.Lua.ORDER_NONE) || 'object';  	
  	argument[2] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
  	argument[3] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';
  	argument[4] = Blockly.Lua.valueToCode(block, 'Z', Blockly.Lua.ORDER_NONE) || 'z';
  	argument[5] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'act';  	
  	argument[6] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'col';  
  	argument[7] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'key';  
  	argument[8] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'zon';    	  	  	
  	argument[9] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'plrvis';  	
  	argument[10] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'ani';  
  	argument[11] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'hea';  
  	argument[12] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'frm';  	
  	argument[13] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'tmr';  	
  	argument[14] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'pdst';  
  	argument[15] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'avd';  
  	var code = 'UpdateEntity(' + argument.join(',') + ')\n';
  	return code;
};


Blockly.Blocks['fpsc_UpdateEntityRT'] = {
	init: function() {
		this.setHelpUrl(helpURL+"UpdateEntityRT");
		this.setColour(functionsColor);
		this.interpolateMsg('UpdateEntityRT', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('OBJECT').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('Z').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('act').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('col').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('key').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('zon').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('plrvis').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('hea').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('frm').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('tmr').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('pdst').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('avd').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("UpdateEntityRT");
	}
};

Blockly.Lua['fpsc_UpdateEntityRT'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  	argument[1] = Blockly.Lua.valueToCode(block, 'OBJECT', Blockly.Lua.ORDER_NONE) || 'object';  	
  	argument[2] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
  	argument[3] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';
  	argument[4] = Blockly.Lua.valueToCode(block, 'Z', Blockly.Lua.ORDER_NONE) || 'z';
  	argument[5] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'act';  	
  	argument[6] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'col';  
  	argument[7] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'key';  
  	argument[8] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'zon';    	  	  	
  	argument[9] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'plrvis';  	
  	argument[10] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'hea';  
  	argument[11] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'frm';  	
  	argument[12] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'tmr';  	
  	argument[13] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'pdst';  
  	argument[14] = Blockly.Lua.valueToCode(block, '', Blockly.Lua.ORDER_NONE) || 'avd';  
  	var code = 'UpdateEntityRT(' + argument.join(',') + ')\n';
  	return code;
};


Blockly.Blocks['fpsc_UpdateEntityAnimatingFlag'] = {
	init: function() {
		this.setHelpUrl(helpURL+"UpdateEntityAnimatingFlag");
		this.setColour(functionsColor);
		this.interpolateMsg('UpdateEntityAnimatingFlag', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('ani').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("");
	}
};

Blockly.Lua['fpsc_UpdateEntityAnimatingFlag'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  	argument[1] = Blockly.Lua.valueToCode(block, 'ANI', Blockly.Lua.ORDER_NONE) || 'ani';  	
  	var code = 'UpdateEntityAnimatingFlag(' + argument.join(',') + ')\n';
  	return code;
};


//-- Macro Functions

Blockly.Blocks['fpsc_GetPlayerDistance'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GetPlayerDistance");
		this.setColour(functionsColor);
		this.interpolateMsg('GetPlayerDistance', ['Number', 'Number', Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		this.setOutput(true, 'Number');		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);		
		this.setTooltip('Returns distance of the player from provided entity (int)');
	}

};
  
  
Blockly.Lua['fpsc_GetPlayerDistance'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  var code = 'GetPlayerDistance' + argument0;
  return [code, Blockly.Lua.ORDER_UNARY];
};


// -- Common Action Functions (called by LUA)

Blockly.Blocks['fpsc_prompt'] = {
  init: function() {
    this.setHelpUrl(helpURL+"Prompt");
    this.setColour(functionsColor);
    this.interpolateMsg('Prompt', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('TEXT').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_prompt'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'Prompt' + argument0 + '\n';
};


Blockly.Blocks['fpsc_PromptDuration'] = {
	init: function() {
		this.setHelpUrl(helpURL+"PromptDuration");
		this.setColour(functionsColor);
		this.interpolateMsg('PromptDuration', ['TEXT', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('TEXT').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("Sets the Duration of the message on the screen");
	}
};

Blockly.Lua['fpsc_PromptDuration'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'TEXT', Blockly.Lua.ORDER_NONE) || 'str';
  	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'VALUE';  	
  	var code = 'PromptDuration(' + argument.join(',') + ')\n';
  	return code;
};

Blockly.Blocks['fpsc_PromptTextSize'] = {
	init: function() {
		this.setHelpUrl(helpURL+"PromptTextSize");
		this.setColour(290);
	    this.appendDummyInput()
	        .appendField("PromptTextSize")
	        .appendField(new Blockly.FieldDropdown([["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"]]), "NAME");
	    this.setPreviousStatement(true);
	    this.setNextStatement(true);
		this.setTooltip("change the size of the prompt text from 1 through 5, 5 being largest");
	}

};

Blockly.Lua['fpsc_PromptTextSize'] = function(block) {
  var argument0 = block.getFieldValue('NAME');
  var code = 'PromptTextSize(' + argument0 +")\n";
  return code;
};


Blockly.Blocks['fpsc_PromptLocal'] = {
	init: function() {
		this.setHelpUrl(helpURL+"PromptLocal");
		this.setColour(functionsColor);
		this.interpolateMsg('PromptLocal', ['TEXT', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('TEXT').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("Sets the Duration of the message on the screen");
	}
};

Blockly.Lua['fpsc_PromptLocal'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  	argument[1] = Blockly.Lua.valueToCode(block, 'TEXT', Blockly.Lua.ORDER_NONE) || 'v';  	
  	var code = 'PromptLocal(' + argument.join(',') + ')\n';
  	return code;
};


Blockly.Blocks['fpsc_SetFogNearest'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetFogNearest");
    this.setColour(functionsColor);
    this.interpolateMsg('SetFogNearest', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	  this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetFogNearest'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetFogNearest' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetFogDistance'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetFogDistance");
    this.setColour(functionsColor);
    this.interpolateMsg('SetFogDistance', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetFogDistance'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetFogDistance' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetFogRed'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetFogRed");
    this.setColour(functionsColor);
    this.interpolateMsg('SetFogRed', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetFogRed'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetFogRed' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetFogGreen'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetFogGreen");
    this.setColour(functionsColor);
    this.interpolateMsg('SetFogGreen', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetFogGreen'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetFogGreen' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetFogBlue'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetFogBlue");
    this.setColour(functionsColor);
    this.interpolateMsg('SetFogBlue', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetFogBlue'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetFogBlue' + argument0 + '\n';
};


Blockly.Blocks['fpsc_SetAmbienceIntensity'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetAmbienceIntensity");
    this.setColour(functionsColor);
    this.interpolateMsg('SetAmbienceIntensity', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetAmbienceIntensity'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetAmbienceIntensity' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetAmbienceRed'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetAmbienceRed");
    this.setColour(functionsColor);
    this.interpolateMsg('SetAmbienceRed', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetAmbienceRed'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetAmbienceRed' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetAmbienceGreen'] = {
  init: function() {
    this.setHelpUrl(helpURL+"");
    this.setColour(functionsColor);
    this.interpolateMsg('', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetAmbienceGreen'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetAmbienceGreen' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetAmbienceBlue'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetAmbienceBlue");
    this.setColour(functionsColor);
    this.interpolateMsg('SetAmbienceBlue', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetAmbienceBlue'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetAmbienceBlue' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetSurfaceRed'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetSurfaceRed");
    this.setColour(functionsColor);
    this.interpolateMsg('SetSurfaceRed', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetSurfaceRed'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetSurfaceRed' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetSurfaceGreen'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetSurfaceGreen");
    this.setColour(functionsColor);
    this.interpolateMsg('SetSurfaceGreen', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetSurfaceGreen'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetSurfaceGreen' + argument0 + '\n';
};


Blockly.Blocks['fpsc_SetSurfaceBlue'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetSurfaceBlue");
    this.setColour(functionsColor);
    this.interpolateMsg('SetSurfaceBlue', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Show text message in the bottom of the screen.");
  }
};  

Blockly.Lua['fpsc_SetSurfaceBlue'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'SetSurfaceBlue' + argument0 + '\n';
};


Blockly.Blocks['fpsc_JumpToLevelIfUsed'] = {
	init: function() {
		this.setHelpUrl(helpURL+"JumpToLevelIfUsed");
		this.setColour(functionsColor);
		this.interpolateMsg('JumpToLevelIfUsed', ['TEXT', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("Sets the Duration of the message on the screen");
	}
};

Blockly.Lua['fpsc_JumpToLevelIfUsed'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  	var code = 'JumpToLevelIfUsed(' + argument.join(',') + ')\n';
  	return code;
};


Blockly.Blocks['fpsc_JumpToLevel'] = {
	init: function() {
		this.setHelpUrl(helpURL+"JumpToLevel");
		this.setColour(functionsColor);
		this.interpolateMsg('JumpToLevel', ['TEXT', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('TEXT').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("Sets the Duration of the message on the screen");
	}
};

Blockly.Lua['fpsc_JumpToLevel'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'TEXT', Blockly.Lua.ORDER_NONE) || 'levelname';
  	var code = 'JumpToLevel(' + argument.join(',') + ')\n';
  	return code;
};


Blockly.Blocks['fpsc_FinishLevel'] = {
	init: function() {
		this.setHelpUrl(helpURL+"FinishLevel");
		this.setColour(290);
		this.interpolateMsg('FinishLevel', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("FinishLevel");
	}

};

Blockly.Lua['fpsc_FinishLevel'] = function(block) {
  var code = 'FinishLevel()' +"\n";
  return code;
};


Blockly.Blocks['fpsc_HideTerrain'] = {
	init: function() {
		this.setHelpUrl(helpURL+"HideTerrain");
		this.setColour(290);
		this.interpolateMsg('HideTerrain', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("stop the terrain drawing");
	}

};

Blockly.Lua['fpsc_HideTerrain'] = function(block) {
  var code = 'HideTerrain()' +"\n";
  return code;
};


Blockly.Blocks['fpsc_ShowTerrain'] = {
	init: function() {
		this.setHelpUrl(helpURL+"ShowTerrain");
		this.setColour(290);
		this.interpolateMsg('ShowTerrain', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("show the terrain again");
	}

};

Blockly.Lua['fpsc_ShowTerrain'] = function(block) {
  var code = 'ShowTerrain()' +"\n";
  return code;
};




Blockly.Blocks['fpsc_HideWater'] = {
	init: function() {
		this.setHelpUrl(helpURL+"HideWater");
		this.setColour(290);
		this.interpolateMsg('HideWater', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("stop water from being drawn");
	}

};

Blockly.Lua['fpsc_HideWater'] = function(block) {
  var code = 'HideWater()' +"\n";
  return code;
};

Blockly.Blocks['fpsc_ShowWater'] = {
	init: function() {
		this.setHelpUrl(helpURL+"ShowWater");
		this.setColour(290);
		this.interpolateMsg('ShowWater', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("show water again");
	}

};

Blockly.Lua['fpsc_ShowWater'] = function(block) {
  var code = 'ShowWater()' +"\n";
  return code;
};

Blockly.Blocks['fpsc_HideHuds'] = {
	init: function() {
		this.setHelpUrl(helpURL+"HideHudsHideHuds");
		this.setColour(290);
		this.interpolateMsg('HideHuds', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("hide huds for weapon and player status");
	}

};

Blockly.Lua['fpsc_HideHuds'] = function(block) {
  var code = 'HideHuds()' +"\n";
  return code;
};

Blockly.Blocks['fpsc_ShowHuds'] = {
	init: function() {
		this.setHelpUrl(helpURL+"ShowHuds");
		this.setColour(290);
		this.interpolateMsg('ShowHuds', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("show huds for weapon and player status");
	}

};

Blockly.Lua['fpsc_ShowHuds'] = function(block) {
  var code = 'ShowHuds()' +"\n";
  return code;
};

Blockly.Blocks['fpsc_FreezeAI'] = {
	init: function() {
		this.setHelpUrl(helpURL+"FreezeAI");
		this.setColour(290);
		this.interpolateMsg('FreezeAI', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("freeze all DarkAI activity used by character AI");
	}

};

Blockly.Lua['fpsc_FreezeAI'] = function(block) {
  var code = 'FreezeAI()' +"\n";
  return code;
};

Blockly.Blocks['fpsc_UnFreezeAI'] = {
	init: function() {
		this.setHelpUrl(helpURL+"UnFreezeAI");
		this.setColour(290);
		this.interpolateMsg('UnFreezeAI', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("unfreeze all DarkAI activity used by character AI");
	}

};

Blockly.Lua['fpsc_UnFreezeAI'] = function(block) {
  var code = 'UnFreezeAI()' +"\n";
  return code;
};

Blockly.Blocks['fpsc_FreezePlayer'] = {
	init: function() {
		this.setHelpUrl(helpURL+"FreezePlayer");
		this.setColour(290);
		this.interpolateMsg('FreezePlayer', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("freeze the ability of the player to move, mouselook or take action");
	}

};

Blockly.Lua['fpsc_FreezePlayer'] = function(block) {
  var code = 'FreezePlayer()' +"\n";
  return code;
};

Blockly.Blocks['fpsc_UnFreezePlayer'] = {
	init: function() {
		this.setHelpUrl(helpURL+"UnFreezePlayer");
		this.setColour(290);
		this.interpolateMsg('UnFreezePlayer', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("unfreeze the ability of the player to move, mouselook or take action");
	}
};

Blockly.Lua['fpsc_UnFreezePlayer'] = function(block) {
  var code = 'UnFreezePlayer()' +"\n";
  return code;
};


Blockly.Blocks['fpsc_SetPlayerHealth'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetPlayerHealth");
		this.setColour(290);
		this.interpolateMsg('SetPlayerHealth', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('assign a new health value to the player (total health).');
	}

};

Blockly.Lua['fpsc_SetPlayerHealth'] = function(block) {
  var argument1 = Blockly.Lua.valueToCode(block, 'VALUE', null) || '"health"';
  var code = 'SetPlayerHealth' + argument1 +"\n";
  return code;
};

Blockly.Blocks['fpsc_SetEntityHealth'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetEntityHealth");
		this.setColour(290);
		this.interpolateMsg('SetEntityHealth', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('can be used to set the health of an entity. If the new health is 0 then it will be destroyed.');
	}

};

Blockly.Lua['fpsc_SetEntityHealth'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'health';	
  var code = 'SetEntityHealth(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_StartParticleEmitter'] = {
	init: function() {
		this.setHelpUrl(helpURL+"StartParticleEmitter");
		this.setColour(290);
		this.interpolateMsg('StartParticleEmitter', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('can be used to set the health of an entity. If the new health is 0 then it will be destroyed.');
	}

};

Blockly.Lua['fpsc_StartParticleEmitter'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  var code = 'StartParticleEmitter(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_StopParticleEmitter'] = {
	init: function() {
		this.setHelpUrl(helpURL+"StopParticleEmitter");
		this.setColour(290);
		this.interpolateMsg('StopParticleEmitter', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('can be used to set the health of an entity. If the new health is 0 then it will be destroyed.');
	}

};

Blockly.Lua['fpsc_StopParticleEmitter'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  var code = 'StopParticleEmitter(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_StartTimer'] = {
  init: function() {
	this.setHelpUrl(helpURL+"StartTimer");
	this.setColour(290);
	this.interpolateMsg('StartTimer',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('resets the timer associated with the specified entity index');
  }
};  

Blockly.Lua['fpsc_StartTimer'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'StartTimer' + argument0 + '\n';
};


Blockly.Blocks['fpsc_GetTimer'] = {
  init: function() {
	this.setHelpUrl(helpURL+"GetTimer");
	this.setColour(290);
	this.interpolateMsg('GetTimer',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.setOutput(true,null)
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setTooltip('get the timer for the entity in millisecon');
  }
};  

Blockly.Lua['fpsc_GetTimer'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'GetTimer' + argument0 + '\n';
};

Blockly.Blocks['fpsc_destroy'] = {
  init: function() {
	this.setHelpUrl(helpURL+"Destroy");
	this.setColour(290);
	this.interpolateMsg('Destroy',['Number', null, Blockly.ALIGN_RIGHT],Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('destroy the specified entity');
  }
};  

Blockly.Lua['fpsc_destroy'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'Destroy' + argument0 + '\n';
};

Blockly.Blocks['fpsc_CollisionOff'] = {
	init: function() {
		this.setHelpUrl(helpURL+"CollisionOff");
		this.setColour(290);
		this.interpolateMsg('CollisionOff', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("turn off physics collision for this entity (handy for a custom entity perhaps dying, where you want to turn off physics so they don't move and play a death animation");
	}

};

Blockly.Lua['fpsc_CollisionOff'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || 'e';
  var code = 'CollisionOff' + argument0 +"\n";
  return code;
};

Blockly.Blocks['fpsc_CollisionOn'] = {
	init: function() {
		this.setHelpUrl(helpURL+"CollisionOn");
		this.setColour(290);
		this.interpolateMsg('CollisionOn', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("restore collision for this entity");
	}

};

Blockly.Lua['fpsc_CollisionOn'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || 'e';
  var code = 'CollisionOn' + argument0 +"\n";
  return code;
};


Blockly.Blocks['fpsc_GetEntityPlayerVisibility'] = {
  init: function() {
	this.setHelpUrl(helpURL+"GetEntityPlayerVisibility");
	this.setColour(290);
	this.interpolateMsg('GetEntityPlayerVisibility',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('GetEntityPlayerVisibility');
  }
};  

Blockly.Lua['fpsc_GetEntityPlayerVisibility'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'GetEntityPlayerVisibility' + argument0 + '\n';
};


Blockly.Blocks['fpsc_Hide'] = {
  init: function() {
	this.setHelpUrl(helpURL+"Hide");
	this.setColour(290);
	this.interpolateMsg('Hide',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('hide the specified entity');
  }
};  

Blockly.Lua['fpsc_Hide'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'Hide' + argument0 + '\n';
};


Blockly.Blocks['fpsc_Show'] = {
  init: function() {
	this.setHelpUrl(helpURL+"Show");
	this.setColour(290);
	this.interpolateMsg('Show',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('show the specified entity');
  }
}; 

Blockly.Lua['fpsc_Show'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'Show' + argument0 + '\n';
};


Blockly.Blocks['fpsc_Spawn'] = {
  init: function() {
	this.setHelpUrl(helpURL+"Spawn");
	this.setColour(290);
	this.interpolateMsg('Spawn',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('if the specified entity was not spawned at start, spawn it');
  }
}; 

Blockly.Lua['fpsc_Spawn'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'Spawn' + argument0 + '\n';
};


Blockly.Blocks['fpsc_SetActivated'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetActivated");
		this.setColour(290);
		this.interpolateMsg('SetActivated', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);					
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('can be used to set the health of an entity. If the new health is 0 then it will be destroyed.');
	}

};

Blockly.Lua['fpsc_SetActivated'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'v';
  var code = 'SetActivated(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_ActivateIfUsed'] = {
	init: function() {
		this.setHelpUrl(helpURL+"ActivateIfUsed");
		this.setColour(290);
		this.interpolateMsg('ActivateIfUsed', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('can be used to set the health of an entity. If the new health is 0 then it will be destroyed.');
	}

};

	
Blockly.Lua['fpsc_ActivateIfUsed'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  var code = 'ActivateIfUsed(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_TransportToIfUsed'] = {
	init: function() {
		this.setHelpUrl(helpURL+"TransportToIfUsed");
		this.setColour(290);
		this.interpolateMsg('GravityOn', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("moves the player to the position of the object in the IFUSED field");
	}

};

Blockly.Lua['fpsc_TransportToIfUsed'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || 'e';
  var code = 'TransportToIfUsed' + argument0 +"\n";
  return code;
};

Blockly.Blocks['fpsc_collected'] = {
  init: function() {
	this.setHelpUrl(helpURL+"Collected");
	this.setColour(290);
	this.interpolateMsg('Collected', ['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('Collected');
  }
};  

Blockly.Lua['fpsc_collected'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'Collected' + argument0 + '\n';
};


Blockly.Blocks['fpsc_MoveUp'] = {
	init: function() {
		this.setHelpUrl(helpURL+"MoveUp");
		this.setColour(330);
		this.interpolateMsg('MoveUp', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		this.appendValueInput('ENTITY').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
	    this.setPreviousStatement(true);
	    this.setNextStatement(true);
		this.setTooltip('move the specified entity upwards by amount');
	}

};  
  
Blockly.Lua['fpsc_MoveUp'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'state';
  	var code = 'MoveUp(' + argument.join(',') + ')\n';
  	return code;
};

Blockly.Blocks['fpsc_Moveforward'] = {
	init: function() {
		this.setHelpUrl(helpURL+"Moveforward");
		this.setColour(330);
		this.interpolateMsg('Moveforward', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		this.appendValueInput('ENTITY').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
	    this.setPreviousStatement(true);
	    this.setNextStatement(true);
		this.setTooltip('move the specified entity forward by amount');
	}

};  
  
Blockly.Lua['fpsc_Moveforward'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'state';
  var code = 'Moveforward(' + argument.join(',') + ')\n';
  return  code;
};

Blockly.Blocks['fpsc_MoveBackward'] = {
	init: function() {
		this.setHelpUrl(helpURL+"MoveBackward");
		this.setColour(330);
		this.interpolateMsg('MoveBackward', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
	    this.setPreviousStatement(true);
	    this.setNextStatement(true);
		this.setTooltip('move the specified entity backward by amount');
	}

};  
  
Blockly.Lua['fpsc_MoveBackward'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'state';
  var code = 'g_MoveBackward(' + argument.join(',') + ')\n';
  return  code;
};

Blockly.Blocks['fpsc_SetPosition'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetPosition");
		this.setColour(290);
		this.interpolateMsg('SetPosition', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('Z').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("sets the absolute position of an entity, use only on entities with physics off (Note: If GravityOff(e) has not been called, positioned object will still align themselves with the terrain floor)");
	}

};

Blockly.Lua['fpsc_SetPosition'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  	argument[1] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
  	argument[2] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';
  	argument[3] = Blockly.Lua.valueToCode(block, 'Z', Blockly.Lua.ORDER_NONE) || 'z';
  var code = 'SetPosition(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_ResetPosition'] = {
	init: function() {
		this.setHelpUrl(helpURL+"ResetPosition");
		this.setColour(290);
		this.interpolateMsg('ResetPosition', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('Z').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("set the absolute rotation of an entity. Only for use on a non physics entity.");
	}

};

Blockly.Lua['fpsc_ResetPosition'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  	argument[1] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
  	argument[2] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';
  	argument[3] = Blockly.Lua.valueToCode(block, 'Z', Blockly.Lua.ORDER_NONE) || 'z';
  	var code = 'ResetPosition(' + argument.join(',') + ')\n';
  	return code;
};


Blockly.Blocks['fpsc_SetRotation'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetRotation");
		this.setColour(290);
		this.interpolateMsg('SetRotation', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('Z').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("set the absolute rotation of an entity. Only for use on a non physics entity.");
	}

};

Blockly.Lua['fpsc_SetRotation'] = function(block) {
	argument = [];
  	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
  	argument[1] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
  	argument[2] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';
  	argument[3] = Blockly.Lua.valueToCode(block, 'Z', Blockly.Lua.ORDER_NONE) || 'z';
  var code = 'SetRotation(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_ModulateSpeed'] = {
	init: function() {
		this.setHelpUrl(helpURL+"ModulateSpeed");
		this.setColour(330);
		this.interpolateMsg('ModulateSpeed', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
	    this.setPreviousStatement(true);
	    this.setNextStatement(true);
		this.setTooltip('alter the move and animation speed of entity');
	}
};  
  
Blockly.Lua['fpsc_ModulateSpeed'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'state';
  var code = 'ModulateSpeed(' + argument.join(',') + ')\n';
  return  code;
};


Blockly.Blocks['fpsc_RotateX'] = {
	init: function() {
		this.setHelpUrl(helpURL+"RotateX");
		this.setColour(290);
		this.interpolateMsg('RotateX', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('ANGLE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);	
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Rotate an entity along the X axis');
	}

};

Blockly.Lua['fpsc_RotateX'] = function(block) {

	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'ANGLE', Blockly.Lua.ORDER_NONE) || 'state';	
  	var code = 'RotateX(' + argument.join(',') + ')\n';
  	return code;
};

Blockly.Blocks['fpsc_RotateY'] = {
	init: function() {
		this.setHelpUrl(helpURL+"RotateY");
		this.setColour(290);
		this.interpolateMsg('RotateY', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('ANGLE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Rotate an entity along the Y axis');
	}

};

Blockly.Lua['fpsc_RotateY'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'ANGLE', Blockly.Lua.ORDER_NONE) || 'state';	
  var code = 'RotateY(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_RotateZ'] = {
	init: function() {
		this.setHelpUrl(helpURL+"RotateZ");
		this.setColour(290);
		this.interpolateMsg('RotateZ', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('ANGLE').setCheck(null).setAlign(Blockly.ALIGN_RIGHT);
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Rotate an entity along the Z axis');
	}

};

Blockly.Lua['fpsc_RotateZ'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'ANGLE', Blockly.Lua.ORDER_NONE) || 'state';	
  var code = 'RotateZ(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_Scale'] = {
	init: function() {
		this.setHelpUrl(helpURL+"Scale");
		this.setColour(290);
		this.interpolateMsg('Scale', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Scales an entity to V%');
	}

};

Blockly.Lua['fpsc_Scale'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
	var code = 'Scale(' + argument.join(',') + ')\n';
  	return code;
};

Blockly.Blocks['fpsc_setanimation'] = {
  init: function() {
	this.setHelpUrl(helpURL+"SetAnimation");
	this.setColour(290);
	this.interpolateMsg('SetAnimation',	['Number', null, Blockly.ALIGN_RIGHT],Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('SetAnimation');
  }
};  

Blockly.Lua['fpsc_setanimation'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'SetAnimation' + argument0 + '\n';
};


Blockly.Blocks['fpsc_SetAnimationFrames'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetAnimationFrames");
		this.setColour(290);
		this.interpolateMsg('SetAnimationFrames', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Scales an entity to V%');
	}

};

Blockly.Lua['fpsc_SetAnimationFrames'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
	var code = 'SetAnimationFrames(' + argument.join(',') + ')\n';
  	return code;
};



Blockly.Blocks['fpsc_playanimation'] = {
  init: function() {
	this.setHelpUrl(helpURL+"PlayAnimation");
	this.setColour(290);
	this.interpolateMsg('PlayAnimation',['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('PlayAnimation');
  }
};  

Blockly.Lua['fpsc_playanimation'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'PlayAnimation' + argument0 + '\n';
};

Blockly.Blocks['fpsc_LoopAnimation'] = {
  init: function() {
	this.setHelpUrl(helpURL+"LoopAnimation");
	this.setColour(290);
	this.interpolateMsg('LoopAnimation',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('loop the animation index or range as previously set');
  }
}; 

Blockly.Lua['fpsc_LoopAnimation'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'LoopAnimation' + argument0 + '\n';
};

Blockly.Blocks['fpsc_StopAnimation'] = {
  init: function() {
	this.setHelpUrl(helpURL+"StopAnimation");
	this.setColour(290);
	this.interpolateMsg('StopAnimation',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('stop the animation for the specified entity');
  }
}; 

Blockly.Lua['fpsc_StopAnimation'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'StopAnimation' + argument0 + '\n';
};


Blockly.Blocks['fpsc_SetAnimationSpeed'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetAnimationSpeed");
		this.setColour(330);
		this.interpolateMsg('SetAnimationSpeed', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
	    this.setPreviousStatement(true);
	    this.setNextStatement(true);
		this.setTooltip('set the animation speed of the entity');
	}

};   
  
Blockly.Lua['fpsc_SetAnimationSpeed'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'state';
  var code = 'SetAnimationSpeed(' + argument.join(',') + ')\n';
  return  code;
};

Blockly.Blocks['fpsc_SetAnimationFrame'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetAnimationFrame");
		this.setColour(290);
		this.interpolateMsg('SetAnimationFrame', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Scales an entity to V%');
	}

};

Blockly.Lua['fpsc_SetAnimationFrame'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
	var code = 'SetAnimationFrame(' + argument.join(',') + ')\n';
  	return code;
};

Blockly.Blocks['fpsc_GetAnimationFrame'] = {
  init: function() {
	this.setHelpUrl(helpURL+"GetAnimationFrame");
	this.setColour(290);
	this.interpolateMsg('GetAnimationFrame',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setOutput(true,null);
	this.setTooltip('get the animation frame number from the entity');
  }
}; 

Blockly.Lua['fpsc_GetAnimationFrame'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'GetAnimationFrame' + argument0 + '\n';
};

Blockly.Blocks['fpsc_CharacterControlUnarmed'] = {
  init: function() {
	this.setHelpUrl(helpURL+"CharacterControlUnarmed");
	this.setColour(290);
	this.interpolateMsg('CharacterControlUnarmed',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('switch character to unarmed state');
  }
}; 

Blockly.Lua['fpsc_CharacterControlUnarmed'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'CharacterControlUnarmed' + argument0 + '\n';
};


Blockly.Blocks['fpsc_CharacterControlLimbo'] = {
  init: function() {
	this.setHelpUrl(helpURL+"CharacterControlLimbo");
	this.setColour(290);
	this.interpolateMsg('CharacterControlLimbo',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('switch character to limbo state');
  }
}; 

Blockly.Lua['fpsc_CharacterControlLimbo'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'CharacterControlLimbo' + argument0 + '\n';
};

Blockly.Blocks['fpsc_CharacterControlArmed'] = {
  init: function() {
	this.setHelpUrl(helpURL+"CharacterControlArmed");
	this.setColour(290);
	this.interpolateMsg('CharacterControlArmed',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('switch character to armed state');
  }
}; 

Blockly.Lua['fpsc_CharacterControlArmed'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'CharacterControlArmed' + argument0 + '\n';
};


Blockly.Blocks['fpsc_CharacterControlFidget'] = {
  init: function() {
	this.setHelpUrl(helpURL+"CharacterControlFidget");
	this.setColour(290);
	this.interpolateMsg('CharacterControlFidget',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('switch character to fidget state');
  }
}; 

Blockly.Lua['fpsc_CharacterControlFidget'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'CharacterControlFidget' + argument0 + '\n';
};

Blockly.Blocks['fpsc_CharacterControlDucked'] = {
  init: function() {
	this.setHelpUrl(helpURL+"CharacterControlDucked");
	this.setColour(290);
	this.interpolateMsg('CharacterControlDucked',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('switch character to crouched state');
  }
}; 

Blockly.Lua['fpsc_CharacterControlDucked'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'CharacterControlDucked' + argument0 + '\n';
};


Blockly.Blocks['fpsc_CharacterControlStand'] = {
  init: function() {
	this.setHelpUrl(helpURL+"CharacterControlStand");
	this.setColour(290);
	this.interpolateMsg('CharacterControlStand',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('switch character to stood state');
  }
}; 

Blockly.Lua['fpsc_CharacterControlStand'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'CharacterControlStand' + argument0 + '\n';
};


Blockly.Blocks['fpsc_SetCharacterToWalk'] = {
  init: function() {
	this.setHelpUrl(helpURL+"SetCharacterToWalk");
	this.setColour(290);
	this.interpolateMsg('SetCharacterToWalk',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('set the character to walk when moving');
  }
}; 


Blockly.Lua['fpsc_SetCharacterToWalk'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'SetCharacterToWalk' + argument0 + '\n';
};


Blockly.Blocks['fpsc_SetCharacterToRun'] = {
  init: function() {
	this.setHelpUrl(helpURL+"SetCharacterToRun");
	this.setColour(290);
	this.interpolateMsg('SetCharacterToRun',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('set the character to run when moving');
  }
}; 

Blockly.Lua['fpsc_SetCharacterToRun'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'SetCharacterToRun' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetCharacterToStrafeLeft'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetCharacterToStrafeLeft");
		this.setColour(290);
		this.interpolateMsg('SetCharacterToStrafeLeft', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('AI to strafe to the left');
	}

};

Blockly.Lua['fpsc_SetCharacterToStrafeLeft'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || 'e';
  var code = 'SetCharacterToStrafeLeft' + argument0 +"\n";
  return code;
};


Blockly.Blocks['fpsc_SetCharacterToStrafeRight'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetCharacterToStrafeRight");
		this.setColour(290);
		this.interpolateMsg('SetCharacterToStrafeRight', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('AI to strafe to the right');
	}

};

Blockly.Lua['fpsc_SetCharacterToStrafeRight'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || 'e';
  var code = 'SetCharacterToStrafeRight' + argument0 +"\n";
  return code;
};




Blockly.Blocks['fpsc_SetCharacterVisionDelay'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetCharacterVisionDelay");
		this.setColour(290);
		this.interpolateMsg('SetCharacterVisionDelay', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Sets the speed at which a ');
	}

};

Blockly.Lua['fpsc_SetCharacterVisionDelay'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
  var code = 'SetCharacterVisionDelay(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_LockCharacterPosition'] = {
	init: function() {
		this.setHelpUrl(helpURL+"LockCharacterPosition");
		this.setColour(290);
		this.interpolateMsg('LockCharacterPosition', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Sets the speed at which a ');
	}

};

Blockly.Lua['fpsc_LockCharacterPosition'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
  var code = 'LockCharacterPosition(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_UnlockCharacterPosition'] = {
	init: function() {
		this.setHelpUrl(helpURL+"UnlockCharacterPosition");
		this.setColour(290);
		this.interpolateMsg('UnlockCharacterPosition', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Sets the speed at which a ');
	}

};

Blockly.Lua['fpsc_UnlockCharacterPosition'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
  var code = 'UnlockCharacterPosition(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_GravityOff'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GravityOff");
		this.setColour(290);
		this.interpolateMsg('GravityOff', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("turns off non physics entities snapping to the terrain floor, handy to call inside _init");
	}

};

Blockly.Lua['fpsc_GravityOff'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || 'e';
  var code = 'GravityOff' + argument0 +"\n";
  return code;
};


Blockly.Blocks['fpsc_GravityOn'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GravityOn");
		this.setColour(290);
		this.interpolateMsg('GravityOn', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("turns on non physics entities snapping to the terrain floor");
	}

};

Blockly.Lua['fpsc_GravityOn'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || 'e';
  var code = 'GravityOn' + argument0 +"\n";
  return code;
};

Blockly.Blocks['fpsc_LookAtPlayer'] = {
	init: function() {
		this.setHelpUrl(helpURL+"LookAtPlayer");
		this.setColour(290);
		this.interpolateMsg('LookAtPlayer', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("turns on non physics entities snapping to the terrain floor");
	}

};

Blockly.Lua['fpsc_LookAtPlayer'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || 'e';
  var code = 'LookAtPlayer' + argument0 +"\n";
  return code;
};


Blockly.Blocks['fpsc_RotateToPlayer'] = {
  init: function() {
	this.setHelpUrl(helpURL+"RotateToPlayer");
	this.setColour(290);
	this.interpolateMsg('RotateToPlayer',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('rotate the specified entity to face the player');
  }
}; 

Blockly.Lua['fpsc_RotateToPlayer'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'RotateToPlayer' + argument0 + '\n';
};

Blockly.Blocks['fpsc_RotateToPlayerSlowly'] = {
	init: function() {
		this.setHelpUrl(helpURL+"RotateToPlayerSlowly");
		this.setColour(290);
		this.interpolateMsg('RotateToPlayerSlowly', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Sets the speed at which a ');
	}

};

Blockly.Lua['fpsc_RotateToPlayerSlowly'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
  var code = 'RotateToPlayerSlowly(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_addplayerweapon'] = {
  init: function() {
	this.setHelpUrl(helpURL+"AddPlayerWeapon");
	this.setColour(290);
	this.interpolateMsg('AddPlayerWeapon',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('AddPlayerWeapon');
  }
};  

Blockly.Lua['fpsc_addplayerweapon'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'AddPlayerWeapon' + argument0 + '\n';
};

Blockly.Blocks['fpsc_ReplacePlayerWeapon'] = {
  init: function() {
	this.setHelpUrl(helpURL+"ReplacePlayerWeapon");
	this.setColour(290);
	this.interpolateMsg('ReplacePlayerWeapon',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('ReplacePlayerWeapon');
  }
};  

Blockly.Lua['fpsc_addplayerweapon'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'ReplacePlayerWeapon' + argument0 + '\n';
};


Blockly.Blocks['fpsc_AddPlayerAmmo'] = {
  init: function() {
	this.setHelpUrl(helpURL+"AddPlayerAmmo");
	this.setColour(290);
	this.interpolateMsg('AddPlayerAmmo',['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('AddPlayerAmmo');
  }
};  

Blockly.Lua['fpsc_AddPlayerAmmo'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'AddPlayerAmmo' + argument0 + '\n';
};


Blockly.Blocks['fpsc_AddPlayerHealth'] = {
	init: function() {
		this.setHelpUrl(helpURL+"AddPlayerHealth");
		this.setColour(290);
		this.interpolateMsg('AddPlayerHealth', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('adds additional health to the player');
	}

};

Blockly.Lua['fpsc_AddPlayerHealth'] = function(block) {
  var argument1 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '"health"';
  var code = 'AddPlayerHealth' + argument1 +"\n";
  return code;
};

Blockly.Blocks['fpsc_SetPlayerLives'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetPlayerLives");
		this.setColour(290);
		this.interpolateMsg('SetPlayerLives', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Sets Player Lives');
	}

};

Blockly.Lua['fpsc_SetPlayerLives'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
  var code = 'SetPlayerLives(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_RemovePlayerWeapons'] = {
	init: function() {
		this.setHelpUrl(helpURL+"RemovePlayerWeapons");
		this.setColour(290);
		this.interpolateMsg('RemovePlayerWeapons', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('adds additional health to the player');
	}

};

Blockly.Lua['fpsc_RemovePlayerWeapons'] = function(block) {
  var argument1 = Blockly.Lua.valueToCode(block, 'VALUE', null) || '"health"';
  var code = 'RemovePlayerWeapons' + argument1 +"\n";
  return code;
};


Blockly.Blocks['fpsc_AddPlayerJetPack'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetPlayerLives");
		this.setColour(290);
		this.interpolateMsg('AddPlayerJetPack', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Sets Player Lives');
	}

};

Blockly.Lua['fpsc_AddPlayerJetPack'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"fuel"';	
  var code = 'AddPlayerJetPack(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_Checkpoint'] = {
  init: function() {
	this.setHelpUrl(helpURL+"Checkpoint");
	this.setColour(290);
	this.interpolateMsg('Checkpoint',['Number', null, Blockly.ALIGN_RIGHT],Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('Checkpoint');
  }
};  

Blockly.Lua['fpsc_Checkpoint'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'Checkpoint' + argument0 + '\n';
};

Blockly.Blocks['fpsc_GetPlayerInZone'] = {
  init: function() {
	this.setHelpUrl(helpURL+"");
	this.setColour(290);
	this.interpolateMsg('GetPlayerInZone',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setOutput(true,null);
	this.setTooltip('return 1 if the player is inside entity zone area');
  }
}; 

Blockly.Lua['fpsc_GetPlayerInZone'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'GetPlayerInZone' + argument0 + '\n';
};


Blockly.Blocks['fpsc_PlaySound'] = {
  init: function() {
	this.setHelpUrl(helpURL+"PlaySound");
	this.setColour(290);
	this.interpolateMsg('PlaySound',['Number', null, Blockly.ALIGN_RIGHT],Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);		
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('PlaySound');
  }
};  

Blockly.Lua['fpsc_PlaySound'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"fuel"';	
  var code = 'PlaySound(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_PlaySoundIfSilent'] = {
	init: function() {
		this.setHelpUrl(helpURL+"PlaySoundIfSilent");
		this.setColour(290);
		this.interpolateMsg('PlaySoundIfSilent', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('plays the entities sound if not playing');
	}

};

Blockly.Lua['fpsc_PlaySoundIfSilent'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
  	var code = 'PlaySoundIfSilent(' + argument.join(',') + ')\n';
  	return code;
};

Blockly.Blocks['fpsc_PlayNon3DSound'] = {
	init: function() {
		this.setHelpUrl(helpURL+"PlayNon3DSound");
		this.setColour(290);
		this.interpolateMsg('PlayNon3DSound', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('play the entities sound as non-3D');
	}

};

Blockly.Lua['fpsc_PlayNon3DSound'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
  	var code = 'PlayNon3DSound(' + argument.join(',') + ')\n';
  	return code;
};


Blockly.Blocks['fpsc_LoopSound'] = {
	init: function() {
		this.setHelpUrl(helpURL+"LoopSound");
		this.setColour(330);
		this.interpolateMsg('LoopSound', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
	    this.setPreviousStatement(true);
	    this.setNextStatement(true);
		this.setTooltip('loop the sound stored in the specified slot');
	}

};  
  
Blockly.Lua['fpsc_LoopSound'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'state';
  var code = 'LoopSound(' + argument.join(',') + ')\n';
  return  code;
};



Blockly.Blocks['fpsc_StopSound'] = {
	init: function() {
		this.setHelpUrl(helpURL+"StopSound");
		this.setColour(330);
		this.interpolateMsg('StopSound', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
	    this.setPreviousStatement(true);
	    this.setNextStatement(true);
		this.setTooltip('stop the sound stored in the specified slot');
	}

};  
  
Blockly.Lua['fpsc_StopSound'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'state';
  	var code = 'StopSound(' + argument.join(',') + ')\n';
  	return  code;
};


Blockly.Blocks['fpsc_SetSoundSpeed'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetSoundSpeed");
    this.setColour(290);
    this.interpolateMsg('SetSoundSpeed', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('TEXT').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("set the speed of the last sound handled in hertz");
  }
};  

Blockly.Lua['fpsc_SetSoundSpeed'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '"freq"';
  return 'SetSoundSpeed' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetSoundVolume'] = {
  init: function() {
    this.setHelpUrl(helpURL+"SetSoundVolume");
    this.setColour(290);
    this.interpolateMsg('SetSoundVolume', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
	this.appendValueInput('TEXT').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("set the volume of the last sound handled 0 to 100");
  }
};  

Blockly.Lua['fpsc_SetSoundVolume'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '"vol"';
  if(argument0 < 0 || argument0 > 100)
  		alert('The value must be between 0 and 100');
  else
  	return 'SetSoundVolume' + argument0 + '\n';
};

Blockly.Blocks['fpsc_PlayVideo'] = {
	init: function() {
		this.setHelpUrl(helpURL+"PlayVideo");
		this.setColour(330);
		this.interpolateMsg('PlayVideo', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
	    this.setPreviousStatement(true);
	    this.setNextStatement(true);
		this.setTooltip('stop the sound stored in the specified slot');
	}

};  
  
Blockly.Lua['fpsc_PlayVideo'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'state';
  	var code = 'PlayVideo(' + argument.join(',') + ')\n';
  	return  code;
};

Blockly.Blocks['fpsc_StopVideo'] = {
	init: function() {
		this.setHelpUrl(helpURL+"StopVideo");
		this.setColour(330);
		this.interpolateMsg('StopVideo', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
	    this.setPreviousStatement(true);
	    this.setNextStatement(true);
		this.setTooltip('stop the sound stored in the specified slot');
	}

};  
  
Blockly.Lua['fpsc_StopVideo'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || 'state';
  	var code = 'StopVideo(' + argument.join(',') + ')\n';
  	return  code;
};


Blockly.Blocks['fpsc_FireWeapon'] = {
  init: function() {
	this.setHelpUrl(helpURL+"FireWeapon");
	this.setColour(290);
	this.interpolateMsg('FireWeapon',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('cause the specified entity to fire at the player');
  }
}; 

Blockly.Lua['fpsc_FireWeapon'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'FireWeapon' + argument0 + '\n';
};

Blockly.Blocks['fpsc_HurtPlayer'] = {
	init: function() {
		this.setHelpUrl(helpURL+"HurtPlayer");
		this.setColour(290);
		this.interpolateMsg('HurtPlayer', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('injure the player, great for custom objects');
	}

};

Blockly.Lua['fpsc_HurtPlayer'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '"state"';	
  var code = 'HurtPlayer(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_SwitchScript'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SwitchScript");
		this.setColour(290);
		this.interpolateMsg('SwitchScript', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('VALUE').setCheck("String").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Switch AI to use a different script');
	}

};

Blockly.Lua['fpsc_SwitchScript'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'ENTITY', Blockly.Lua.ORDER_NONE) || 'e';
	argument[1] = Blockly.Lua.valueToCode(block, 'VALUE', Blockly.Lua.ORDER_NONE) || '';
  var code = 'SwitchScript(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_SetCharacterSoundSet'] = {
  init: function() {
	this.setHelpUrl(helpURL+"SetCharacterSoundSet");
	this.setColour(290);
	this.interpolateMsg('SetCharacterSoundSet',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('cause the specified entity to fire at the player');
  }
}; 

Blockly.Lua['fpsc_SetCharacterSound'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'SetCharacterSoundSet' + argument0 + '\n';
};

Blockly.Blocks['fpsc_SetCharacterSound'] = {
  init: function() {
	this.setHelpUrl(helpURL+"SetCharacterSound");
	this.setColour(290);
	this.interpolateMsg('SetCharacterSound',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('cause the specified entity to fire at the player');
  }
}; 

Blockly.Lua['fpsc_SetCharacterSound'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'SetCharacterSound' + argument0 + '\n';
};


Blockly.Blocks['fpsc_PlayCharacterSound'] = {
  init: function() {
	this.setHelpUrl(helpURL+"PlayCharacterSound");
	this.setColour(290);
	this.interpolateMsg('PlayCharacterSound',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('cause the specified entity to fire at the player');
  }
}; 

Blockly.Lua['fpsc_PlayCharacterSound'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'PlayCharacterSound' + argument0 + '\n';
};

Blockly.Blocks['fpsc_PlayCombatMusic'] = {
	init: function() {
		this.setHelpUrl(helpURL+"PlayCombatMusic");
		this.setColour(290);
		this.interpolateMsg('PlayCombatMusic', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('PLAYTIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('FADETIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Plays the combat music');
	}

};

Blockly.Lua['fpsc_PlayCombatMusic'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'PLAYTIME', Blockly.Lua.ORDER_NONE) || 'PLAYTIME';
	argument[1] = Blockly.Lua.valueToCode(block, 'FADETIME', Blockly.Lua.ORDER_NONE) || 'FADETIME';		
  var code = 'PlayCombatMusic(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_PlayFinalAssaultMusic'] = {
	init: function() {
		this.setHelpUrl(helpURL+"PlayFinalAssaultMusic");
		this.setColour(290);
		this.interpolateMsg('PlayFinalAssaultMusic', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('FADETIME').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Plays the assault music');
	}

};

Blockly.Lua['fpsc_PlayFinalAssaultMusic'] = function(block) {
  var argument1 = Blockly.Lua.valueToCode(block, 'FADETIME', null) || 'fadetime';
  var code = 'PlayFinalAssaultMusic' + argument1 +"\n";
  return code;
};

Blockly.Blocks['fpsc_DisableMusicReset'] = {
	init: function() {
		this.setHelpUrl(helpURL+"DisableMusicReset");
		this.setColour(290);
		this.interpolateMsg('DisableMusicReset', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('VALUE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('call at start of game to suspend normal music restart behaviour');
	}

};

Blockly.Lua['fpsc_DisableMusicReset'] = function(block) {
  var argument1 = Blockly.Lua.valueToCode(block, 'VALUE', null);
  var code = 'DisableMusicReset(' + argument1 +")\n";
  return code;
};

Blockly.Blocks['fpsc_HideLight'] = {
  init: function() {
	this.setHelpUrl(helpURL+"HideLight");
	this.setColour(290);
	this.interpolateMsg('HideLight',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('cause the specified entity to fire at the player');
  }
}; 

Blockly.Lua['fpsc_HideLight'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'HideLight' + argument0 + '\n';
};

Blockly.Blocks['fpsc_ShowLight'] = {
  init: function() {
	this.setHelpUrl(helpURL+"ShowLight");
	this.setColour(290);
	this.interpolateMsg('ShowLight',	['Number', null, Blockly.ALIGN_RIGHT],	Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('cause the specified entity to fire at the player');
  }
}; 

Blockly.Lua['fpsc_ShowLight'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'ShowLight' + argument0 + '\n';
};

Blockly.Blocks['fpsc_LoadImages'] = {
	init: function() {
		this.setHelpUrl(helpURL+"LoadImages");
		this.setColour(290);
		this.interpolateMsg('LoadImages', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);	
		this.appendValueInput('TEXT').setCheck("text").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('NUM').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("specify a folder inside 'scriptbank\images\' to load images at slot V onwards");
	}

};

Blockly.Lua['fpsc_LoadImages'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'TEXT', Blockly.Lua.ORDER_NONE) || '';
	argument[1] = Blockly.Lua.valueToCode(block, 'NUM', Blockly.Lua.ORDER_NONE) || '';		
  var code = 'LoadImages(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_SetImagePosition'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetImagePosition");
		this.setColour(290);
		this.interpolateMsg('SetImagePosition', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);	
		this.appendValueInput('X').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('Y').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("set the X and Y position of where the image should be drawn");
	}

};

Blockly.Lua['fpsc_SetImagePosition'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || '';
	argument[1] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || '';	  
	var code = 'SetImagePosition(' + argument.join(',') + ')\n';
 	return code;
};

Blockly.Blocks['fpsc_ShowImage'] = {
	init: function() {
		this.setHelpUrl(helpURL+"ShowImage");
		this.setColour(290);
		this.interpolateMsg('SetImagePosition', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);	
		this.appendValueInput('IMG').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("select the image slot number to be drawn");
	}

};

Blockly.Lua['fpsc_ShowImage'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'IMG', null);
  var code = 'ShowImage' + argument0  +"\n";
  return code;
};

Blockly.Blocks['fpsc_HideImage'] = {
	init: function() {
		this.setHelpUrl(helpURL+"HideImage");
		this.setColour(290);
		this.interpolateMsg('HideImage', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);	
		this.appendValueInput('IMG').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("hide the image from being drawn");
	}

};

Blockly.Lua['fpsc_HideImage'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'IMG', null);
  var code = 'HideImage' + argument0  +"\n";
  return code;
};


Blockly.Blocks['fpsc_SetImageAlignment'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetImageAlignment");
		this.setColour(290);
		this.interpolateMsg('SetImageAlignment', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);	
		this.appendValueInput('IMG').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("use 0 to position image using center hotspot, 1 is top left hotspot");
	}

};

Blockly.Lua['fpsc_SetImageAlignment'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'IMG', null);
  var code = 'SetImageAlignment' + argument0  +"\n";
  return code;
};


Blockly.Blocks['fpsc_Text'] = {
	init: function() {
		this.setHelpUrl(helpURL+"Text");
		this.setColour(290);
		this.interpolateMsg('Text', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('SIZE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('TXT').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Plays the combat music');
	}

};

Blockly.Lua['fpsc_Text'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
	argument[1] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';		
  	argument[2] = Blockly.Lua.valueToCode(block, 'SIZE', Blockly.Lua.ORDER_NONE) || 'size';
	argument[3] = Blockly.Lua.valueToCode(block, 'TXT', Blockly.Lua.ORDER_NONE) || 'txt';		
  var code = 'Text(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_TextCenterOnX'] = {
	init: function() {
		this.setHelpUrl(helpURL+"TextCenterOnX");
		this.setColour(290);
		this.interpolateMsg('TextCenterOnX', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('SIZE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('TXT').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Plays the combat music');
	}

};

Blockly.Lua['fpsc_TextCenterOnX'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
	argument[1] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';		
  	argument[2] = Blockly.Lua.valueToCode(block, 'SIZE', Blockly.Lua.ORDER_NONE) || 'size';
	argument[3] = Blockly.Lua.valueToCode(block, 'TXT', Blockly.Lua.ORDER_NONE) || 'txt';		
  var code = 'TextCenterOnX(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_TextColor'] = {
	init: function() {
		this.setHelpUrl(helpURL+"TextColor");
		this.setColour(290);
		this.interpolateMsg('TextColor', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('SIZE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('TXT').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('R').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('G').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('B').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Plays the combat music');
	}

};

Blockly.Lua['fpsc_TextColor'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
	argument[1] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';		
  	argument[2] = Blockly.Lua.valueToCode(block, 'SIZE', Blockly.Lua.ORDER_NONE) || 'size';
	argument[3] = Blockly.Lua.valueToCode(block, 'TXT', Blockly.Lua.ORDER_NONE) || 'txt';
	argument[4] = Blockly.Lua.valueToCode(block, 'R', Blockly.Lua.ORDER_NONE) || 'r';		
  	argument[5] = Blockly.Lua.valueToCode(block, 'G', Blockly.Lua.ORDER_NONE) || 'g';
	argument[6] = Blockly.Lua.valueToCode(block, 'B', Blockly.Lua.ORDER_NONE) || 'b';				
  var code = 'TextColor(' + argument.join(',') + ')\n';
  return code;
};

Blockly.Blocks['fpsc_TextCenterOnXColor'] = {
	init: function() {
		this.setHelpUrl(helpURL+"TextCenterOnXColor");
		this.setColour(290);
		this.interpolateMsg('TextCenterOnXColor', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('SIZE').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('TXT').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('R').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('G').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('B').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Plays the combat music');
	}

};

Blockly.Lua['fpsc_TextCenterOnXColor'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
	argument[1] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';		
  	argument[2] = Blockly.Lua.valueToCode(block, 'SIZE', Blockly.Lua.ORDER_NONE) || 'size';
	argument[3] = Blockly.Lua.valueToCode(block, 'TXT', Blockly.Lua.ORDER_NONE) || 'txt';
	argument[4] = Blockly.Lua.valueToCode(block, 'R', Blockly.Lua.ORDER_NONE) || 'r';		
  	argument[5] = Blockly.Lua.valueToCode(block, 'G', Blockly.Lua.ORDER_NONE) || 'g';
	argument[6] = Blockly.Lua.valueToCode(block, 'B', Blockly.Lua.ORDER_NONE) || 'b';				
  var code = 'TextCenterOnXColor(' + argument.join(',') + ')\n';
  return code;
};


Blockly.Blocks['fpsc_Panel'] = {
	init: function() {
		this.setHelpUrl(helpURL+"Panel");
		this.setColour(290);
		this.interpolateMsg('Panel', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.appendValueInput('X').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('Y').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('X2').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);			
		this.appendValueInput('Y2').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Plays the combat music');
	}

};

Blockly.Lua['fpsc_Panel'] = function(block) {
	var argument = [];	
	argument[0] = Blockly.Lua.valueToCode(block, 'X', Blockly.Lua.ORDER_NONE) || 'x';
	argument[1] = Blockly.Lua.valueToCode(block, 'Y', Blockly.Lua.ORDER_NONE) || 'y';		
  	argument[2] = Blockly.Lua.valueToCode(block, 'X2', Blockly.Lua.ORDER_NONE) || 'x2';
	argument[3] = Blockly.Lua.valueToCode(block, 'Y2', Blockly.Lua.ORDER_NONE) || 'y2';
  var code = 'Panel(' + argument.join(',') + ')\n';
  return code;
};


// -- Common Multiplayer

Blockly.Blocks['fpsc_MP_IsServer'] = {
	init: function() {
		this.setHelpUrl(helpURL+"MP_IsServer");
		this.setColour(290);
		this.interpolateMsg('MP_IsServer', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("MP_IsServer");
	}

};

Blockly.Lua['fpsc_MP_IsServer'] = function(block) {
  return 'MP_IsServer()' +"\n";
};


Blockly.Blocks['fpsc_SetMultiplayerGameMode'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetMultiplayerGameMode");
		this.setColour(290);
		this.interpolateMsg('SetMultiplayerGameMode', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);	
		this.appendValueInput('MODE').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("hide the image from being drawn");
	}

};

Blockly.Lua['fpsc_SetMultiplayerGameMode'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'MODE', null);
  var code = 'HideImage' + argument0  +"\n";
  return code;
};

Blockly.Blocks['fpsc_GetMultiplayerGameMode'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GetMultiplayerGameMode");
		this.setColour(290);
		this.interpolateMsg('GetMultiplayerGameMode', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);	
		this.appendValueInput('MODE').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("hide the image from being drawn");
	}

};

Blockly.Lua['fpsc_GetMultiplayerGameMode'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'MODE', null);
  var code = 'HideImage' + argument0  +"\n";
  return code;
};

Blockly.Blocks['fpsc_SetServerTimer'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetServerTimer");
		this.setColour(290);
		this.interpolateMsg('SetServerTimer', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);	
		this.appendValueInput('T').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("hide the image from being drawn");
	}

};

Blockly.Lua['fpsc_SetServerTimer'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'T', null);
  var code = 'SetServerTimer' + argument0  +"\n";
  return code;
};


Blockly.Blocks['fpsc_GetServerTimer'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GetServerTimer");
		this.setColour(290);
		this.interpolateMsg('GetServerTimer', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);	
		this.appendValueInput('T').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("hide the image from being drawn");
	}

};

Blockly.Lua['fpsc_GetServerTimer'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'T', null);
  var code = 'GetServerTimer' + argument0  +"\n";
  return code;
};

Blockly.Blocks['fpsc_GetServerTimerPassed'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GetServerTimerPassed");
		this.setColour(290);
		this.interpolateMsg('GetServerTimerPassed', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("GetServerTimerPassed");
	}

};

Blockly.Lua['fpsc_GetServerTimerPassed'] = function(block) {
  return 'GetServerTimerPassed()' +"\n";
};

Blockly.Blocks['fpsc_ServerRespawnAll'] = {
	init: function() {
		this.setHelpUrl(helpURL+"ServerRespawnAll");
		this.setColour(290);
		this.interpolateMsg('ServerRespawnAll', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("ServerRespawnAll");
	}

};

Blockly.Lua['fpsc_ServerRespawnAll'] = function(block) {
  return 'ServerRespawnAll()' +"\n";
};


Blockly.Blocks['fpsc_ServerEndPlay'] = {
	init: function() {
		this.setHelpUrl(helpURL+"ServerEndPlay");
		this.setColour(290);
		this.interpolateMsg('ServerEndPlay', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("ServerEndPlay");
	}

};

Blockly.Lua['fpsc_ServerEndPlay'] = function(block) {
  return 'ServerEndPlay()' +"\n";
};


Blockly.Blocks['fpsc_GetShowScores'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GetShowScores");
		this.setColour(290);
		this.interpolateMsg('GetShowScores', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("GetShowScores");
	}

};

Blockly.Lua['fpsc_GetShowScores'] = function(block) {
  return 'GetShowScores()' +"\n";
};


Blockly.Blocks['fpsc_SetServerKillsToWin'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetServerKillsToWin");
		this.setColour(290);
		this.interpolateMsg('SetServerKillsToWin', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);	
		this.appendValueInput('T').setCheck("number").setAlign(Blockly.ALIGN_RIGHT);				
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("hide the image from being drawn");
	}

};

Blockly.Lua['fpsc_SetServerKillsToWin'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'T', null);
  var code = 'SetServerKillsToWin' + argument0  +"\n";
  return code;
};

Blockly.Blocks['fpsc_GetScancode'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GetScancode");
		this.setColour(290);
		this.interpolateMsg('GetScancode', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("GetScancode");
	}

};

Blockly.Lua['fpsc_GetScancode'] = function(block) {
  return 'GetScancode()' +"\n";
};

Blockly.Blocks['fpsc_GetMultiplayerTeamBased'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GetMultiplayerTeamBased");
		this.setColour(290);
		this.interpolateMsg('GetMultiplayerTeamBased', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("GetMultiplayerTeamBased");
	}

};

Blockly.Lua['fpsc_GetMultiplayerTeamBased'] = function(block) {
  return 'GetMultiplayerTeamBased()' +"\n";
};

Blockly.Blocks['fpsc_SetMultiplayerGameFriendlyFireOff'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetMultiplayerGameFriendlyFireOff");
		this.setColour(290);
		this.interpolateMsg('SetMultiplayerGameFriendlyFireOff', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("SetMultiplayerGameFriendlyFireOff");
	}

};

Blockly.Lua['fpsc_SetMultiplayerGameFriendlyFireOff'] = function(block) {
  return 'SetMultiplayerGameFriendlyFireOff()' +"\n";
};


Blockly.Blocks['fpsc_SetNameplatesOff'] = {
	init: function() {
		this.setHelpUrl(helpURL+"SetNameplatesOff");
		this.setColour(290);
		this.interpolateMsg('SetNameplatesOff', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("SetNameplatesOff");
	}

};

Blockly.Lua['fpsc_SetNameplatesOff'] = function(block) {
  return 'SetNameplatesOff()' +"\n";
};

Blockly.Blocks['fpsc_GetCoOpMode'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GetCoOpMode");
		this.setColour(290);
		this.interpolateMsg('GetCoOpMode', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("GetCoOpMode");
	}

};

Blockly.Lua['fpsc_GetCoOpMode'] = function(block) {
  return 'GetCoOpMode()' +"\n";
};

Blockly.Blocks['fpsc_GetCoOpEnemiesAlive'] = {
	init: function() {
		this.setHelpUrl(helpURL+"GetCoOpEnemiesAlive");
		this.setColour(290);
		this.interpolateMsg('GetCoOpEnemiesAlive', [null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);		
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip("GetCoOpEnemiesAlive");
	}

};

Blockly.Lua['fpsc_GetCoOpEnemiesAlive'] = function(block) {
  return 'GetCoOpEnemiesAlive()' +"\n";
};


// --- FIELDS

Blockly.Blocks['field_angle'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.appendDummyInput().appendField(new Blockly.FieldAngle("90"), "NAME");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip('');
  }
};

Blockly.Lua['field_angle'] = function(block) {
  var code =  block.getFieldValue('NAME');
  return [code, Blockly.Lua.ORDER_NONE];
};


// -- SCRIPT BLOCKS

Blockly.Blocks['fpsc_Include'] = {
  init: function() {
    this.setHelpUrl(helpURL+"Include");
    this.setColour(290);
    this.interpolateMsg('Include', ['TEXT', null, Blockly.ALIGN_RIGHT],  Blockly.ALIGN_RIGHT);
    this.appendValueInput('TEXT').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip("Use within the init function to ensure the script is pre loaded");
  }
};  

Blockly.Lua['fpsc_Include'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'TEXT', null) || '""';
  return 'Include' + argument0 + '\n';
};



// UNKNOWN OR DEPRECIETED BLOCKS
 
Blockly.Blocks['fpsc_winzone'] = {
  init: function() {
	this.setHelpUrl(helpURL+"WinZone");
	this.setColour(290);
	this.interpolateMsg('WinZone',['Number', null, Blockly.ALIGN_RIGHT],Blockly.ALIGN_RIGHT);
	this.appendValueInput('ENTITY').setCheck('Number').setAlign(Blockly.ALIGN_RIGHT);
	this.setPreviousStatement(true);
	this.setNextStatement(true);
	this.setTooltip('WinZone');
  }
};  

Blockly.Lua['fpsc_winzone'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || '""';
  return 'WinZone' + argument0 + '\n';
};