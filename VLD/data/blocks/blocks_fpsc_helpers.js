Blockly.Blocks['fpsc_entity'] = {
  init: function() {
    this.setHelpUrl(null);
    this.setColour(230);
    this.interpolateMsg('entity', ['Number', null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
	this.setOutput(true, 'Number');
    this.setTooltip('Current ');
  }
}; 
  
Blockly.Lua['fpsc_entity'] = function(block) {
  return ['e', Blockly.Lua.ORDER_UNARY];
};

  

Blockly.Blocks['vlh_entityattr'] = {
	init: function() {
		this.setHelpUrl(null);
		this.setColour(330);
		this.interpolateMsg('GetEntityAttr',
                        [null, null, Blockly.ALIGN_RIGHT],
                        Blockly.ALIGN_RIGHT);
		this.setOutput(true, null);
		
		this.appendValueInput('ENTITY')
			.setCheck("Number")
			.setAlign(Blockly.ALIGN_RIGHT);
			
		this.appendValueInput('VALUE')
			.setCheck("String")
			.setAlign(Blockly.ALIGN_RIGHT);
		
		this.setTooltip('Returns entity attribute (mixed)');
	}

};  
  
Blockly.Lua['vlh_entityattr'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || 'e';
  var argument1 = Blockly.Lua.valueToCode(block, 'VALUE', null) || '"state"';
  var code = 'g_Entity[' + argument0+']['+argument1+']';
  return [code, Blockly.Lua.ORDER_UNARY];
};

Blockly.Blocks['vlh_setentityattr'] = {
	init: function() {
		this.setHelpUrl(null);
		this.setColour(290);
		this.interpolateMsg('SetEntityAttr',[null, null, Blockly.ALIGN_RIGHT], Blockly.ALIGN_RIGHT);
		//this.setOutput(true, null);		
		this.appendValueInput('ENTITY').setCheck("Number").setAlign(Blockly.ALIGN_RIGHT);	
		this.appendValueInput('STRING').setCheck("String").setAlign(Blockly.ALIGN_RIGHT);
		this.appendValueInput('VALUE').setCheck(false).setAlign(Blockly.ALIGN_RIGHT);			
		this.setPreviousStatement(true);
		this.setNextStatement(true);		
		this.setTooltip('Sets entity attribute (mixed)');
	}

};


Blockly.Lua['vlh_setentityattr'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'ENTITY', null) || 'e';
  var argument1 = Blockly.Lua.valueToCode(block, 'VALUE', null) || '"state"';
  var argument2 = Blockly.Lua.valueToCode(block, 'VALUE', null) || '""';
  var code = 'g_Entity[' + argument0+']['+argument1+'] = '+argument2+"\n";
  return code;
};

Blockly.Blocks['vlh_type'] = {
	init: function() {
		this.setHelpUrl(null);
		this.setColour(160);
		this.interpolateMsg('type of',
                        ["String", null, Blockly.ALIGN_RIGHT],
                        Blockly.ALIGN_RIGHT);
		this.setOutput(true, "String");
		
		this.appendValueInput('VALUE')
			.setCheck(null)
			.setAlign(Blockly.ALIGN_RIGHT);
		
		this.setTooltip('Returns variable type name (string)');
	}

};
  
  
Blockly.Lua['vlh_type'] = function(block) {
  var argument0 = Blockly.Lua.valueToCode(block, 'VALUE', null) || '""';
  var code = 'type' + argument0;
  return [code, Blockly.Lua.ORDER_UNARY];
};



Blockly.Blocks['vlh_keypressede'] = {
	init: function() {
		this.setHelpUrl(null);
		this.setColour(210);
		this.interpolateMsg('key E pressed',
                        ["Bool", null, Blockly.ALIGN_RIGHT],
                        Blockly.ALIGN_RIGHT);
		this.setOutput(true, "Boolean");
		
		this.setTooltip('Checks if key "E" is pressed (bool)');
	}

};
  
  
Blockly.Lua['vlh_keypressede'] = function(block) {
  var code = 'g_KeyPressE == 1';
  return [code, Blockly.Lua.ORDER_UNARY];
};



Blockly.Blocks.controls_if = {
    init: function() {
        this.setHelpUrl(Blockly.Msg.CONTROLS_IF_HELPURL);
        this.setColour(Blockly.Blocks.logic.HUE);
        this.appendValueInput("IF0").setCheck("Boolean").appendField(Blockly.Msg.CONTROLS_IF_MSG_IF);
        this.appendStatementInput("DO0").appendField(Blockly.Msg.CONTROLS_IF_MSG_THEN);
        this.setPreviousStatement(!0);
        this.setNextStatement(!0);
        this.setMutator(new Blockly.Mutator(["controls_if_elseif", "controls_if_else"]));
        var a = this;
        this.setTooltip(function() {
            if (a.elseifCount_ || a.elseCount_) {
                if (!a.elseifCount_ &&
                    a.elseCount_) return Blockly.Msg.CONTROLS_IF_TOOLTIP_2;
                if (a.elseifCount_ && !a.elseCount_) return Blockly.Msg.CONTROLS_IF_TOOLTIP_3;
                if (a.elseifCount_ && a.elseCount_) return Blockly.Msg.CONTROLS_IF_TOOLTIP_4
            } else return Blockly.Msg.CONTROLS_IF_TOOLTIP_1;
            return ""
        });
        this.elseCount_ = this.elseifCount_ = 0
    },
    mutationToDom: function() {
        if (!this.elseifCount_ && !this.elseCount_) return null;
        var a = document.createElement("mutation");
        this.elseifCount_ && a.setAttribute("elseif", this.elseifCount_);
        this.elseCount_ && a.setAttribute("else",
            1);
        return a
    },
    domToMutation: function(a) {
        this.elseifCount_ = parseInt(a.getAttribute("elseif"), 10);
        this.elseCount_ = parseInt(a.getAttribute("else"), 10);
        for (a = 1; a <= this.elseifCount_; a++) this.appendValueInput("IF" + a).setCheck("Boolean").appendField(Blockly.Msg.CONTROLS_IF_MSG_ELSEIF), this.appendStatementInput("DO" + a).appendField(Blockly.Msg.CONTROLS_IF_MSG_THEN);
        this.elseCount_ && this.appendStatementInput("ELSE").appendField(Blockly.Msg.CONTROLS_IF_MSG_ELSE)
    },
    decompose: function(a) {
        var b = Blockly.Block.obtain(a,
            "controls_if_if");
        b.initSvg();
        for (var c = b.getInput("STACK").connection, d = 1; d <= this.elseifCount_; d++) {
            var e = Blockly.Block.obtain(a, "controls_if_elseif");
            e.initSvg();
            c.connect(e.previousConnection);
            c = e.nextConnection
        }
        this.elseCount_ && (a = Blockly.Block.obtain(a, "controls_if_else"), a.initSvg(), c.connect(a.previousConnection));
        return b
    },
    compose: function(a) {
        this.elseCount_ && this.removeInput("ELSE");
        this.elseCount_ = 0;
        for (var b = this.elseifCount_; 0 < b; b--) this.removeInput("IF" + b), this.removeInput("DO" + b);
        this.elseifCount_ =
            0;
        for (a = a.getInputTargetBlock("STACK"); a;) {
            switch (a.type) {
                case "controls_if_elseif":
                    this.elseifCount_++;
                    var b = this.appendValueInput("IF" + this.elseifCount_).setCheck("Boolean").appendField(Blockly.Msg.CONTROLS_IF_MSG_ELSEIF),
                        c = this.appendStatementInput("DO" + this.elseifCount_);
                    c.appendField(Blockly.Msg.CONTROLS_IF_MSG_THEN);
                    a.valueConnection_ && b.connection.connect(a.valueConnection_);
                    a.statementConnection_ && c.connection.connect(a.statementConnection_);
                    break;
                case "controls_if_else":
                    this.elseCount_++;
                    b = this.appendStatementInput("ELSE");
                    b.appendField(Blockly.Msg.CONTROLS_IF_MSG_ELSE);
                    a.statementConnection_ && b.connection.connect(a.statementConnection_);
                    break;
                default:
                    throw "Unknown block type.";
            }
            a = a.nextConnection && a.nextConnection.targetBlock()
        }
    },
    saveConnections: function(a) {
        a = a.getInputTargetBlock("STACK");
        for (var b = 1; a;) {
            switch (a.type) {
                case "controls_if_elseif":
                    var c = this.getInput("IF" + b),
                        d = this.getInput("DO" + b);
                    a.valueConnection_ = c && c.connection.targetConnection;
                    a.statementConnection_ = d && d.connection.targetConnection;
                    b++;
                    break;
                case "controls_if_else":
                    d = this.getInput("ELSE");
                    a.statementConnection_ = d && d.connection.targetConnection;
                    break;
                default:
                    throw "Unknown block type.";
            }
            a = a.nextConnection && a.nextConnection.targetBlock()
        }
    }
};
Blockly.Blocks.controls_if_if = {
    init: function() {
        this.setColour(Blockly.Blocks.logic.HUE);
        this.appendDummyInput().appendField(Blockly.Msg.CONTROLS_IF_IF_TITLE_IF);
        this.appendStatementInput("STACK");
        this.setTooltip(Blockly.Msg.CONTROLS_IF_IF_TOOLTIP);
        this.contextMenu = !1
    }
};
Blockly.Blocks.controls_if_elseif = {
    init: function() {
        this.setColour(Blockly.Blocks.logic.HUE);
        this.appendDummyInput().appendField(Blockly.Msg.CONTROLS_IF_ELSEIF_TITLE_ELSEIF);
        this.setPreviousStatement(!0);
        this.setNextStatement(!0);
        this.setTooltip(Blockly.Msg.CONTROLS_IF_ELSEIF_TOOLTIP);
        this.contextMenu = !1
    }
};
Blockly.Blocks.controls_if_else = {
    init: function() {
        this.setColour(Blockly.Blocks.logic.HUE);
        this.appendDummyInput().appendField(Blockly.Msg.CONTROLS_IF_ELSE_TITLE_ELSE);
        this.setPreviousStatement(!0);
        this.setTooltip(Blockly.Msg.CONTROLS_IF_ELSE_TOOLTIP);
        this.contextMenu = !1
    }
};