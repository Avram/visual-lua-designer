﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Windows.Forms;

namespace VLD 
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public class VLDTab : TabPage
    {
        private WebBrowser browser;
        private String file_to_load = "";

        public VLDTab() : base()
        {
            this.browser = new WebBrowser();
            this.browser.AllowNavigation = false;
            this.browser.AllowWebBrowserDrop = false;
            this.browser.IsWebBrowserContextMenuEnabled = false;
            this.browser.ScriptErrorsSuppressed = true;
            this.browser.ScrollBarsEnabled = false;
            this.browser.Dock = DockStyle.Fill;


            String vldpath = Program.exedir + @"\data\vld.html";

            if (!File.Exists(vldpath))
                vldpath = "d:\\Dropbox\\fpsc visual lua creator desktop\\client\\vld.html";

            //webBrowser1.Navigate("http://" + host);
            this.browser.Navigate("file:///" + vldpath);
            this.browser.ObjectForScripting = this;
            this.browser.DocumentCompleted += browser_DocumentCompleted;

            this.Controls.Add(this.browser);

        }

        public VLDTab(String filepath) : this()
        {
            this.file_to_load = filepath;
        }

        void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (!this.file_to_load.Equals(""))
            {
                Object[] args = new Object[1];
                args[0] = this.file_to_load;
                this.invokeJS("loadXML", args);
            }
        }

        public void invokeJS(String fname, object[] args)
        {
            this.browser.Document.InvokeScript(fname, args);
        }

        public void invokeJS(String fname)
        {
            Object[] args = new Object[0];
            invokeJS(fname, args);
        }
    }
}
