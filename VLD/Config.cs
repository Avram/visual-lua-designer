﻿using System;
using System.IO;
using System.Windows.Forms;

namespace VLD
{
    public class Config
    {
        public String path, exedir;
        public IniParser settings;
        private static Config instance;

        private Config(String inifile)
        {
            this.path = Application.ExecutablePath;
            this.exedir = Application.StartupPath;
            //this.serverpath = path + "client\\visual-lua-server.exe";

            if (File.Exists(this.exedir + inifile))
            {
                this.settings = new IniParser(@"" + this.exedir + "\\" + inifile);
            }
            else
            {
                File.Create(exedir + inifile);
                this.settings = new IniParser(@"" + this.exedir + "\\" + inifile);
            }
        }

        public static Config getInstance(String filename)
        {
            if (Config.instance == null)
            {
                Config.instance = new Config(filename);
            }
                
            return Config.instance;
        }

        public static Config getInstance()
        {
            return Config.getInstance("config.ini");
        }

        public string get(String option)
        {
            return this.settings.GetSetting("settings", option);
        }

        public string get(String section, String option)
        {
            return this.settings.GetSetting(section, option);
        }
    }
}
